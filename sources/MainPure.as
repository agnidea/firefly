/**
 * Created by flower on 07.10.14.
 */
package {
import flash.display.Sprite;
import flash.events.Event;

import ru.agnidea.firefly.Core;

[SWF(width="960", height="640", frameRate="60", backgroundColor="#4a4137")]
public class MainPure extends Sprite {
    public function MainPure() {
        trace("main sprite");
        new Core();

        addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
    }

    private function addedToStageHandler(event:Event):void {
//        trace("main addedToStageHandler");
        Core.start(this.stage);
    }
}
}
