package {
import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.geom.Rectangle;

import net.hires.debug.Stats;

import sk.yoz.ycanvas.map.demo.DemoMain;

import starling.core.Starling;

/**
 * This is a common application wrapper used in feathers examples.
 * The MainFlex class is reused from ApplicationMap project.
 */
[SWF(width="960", height="640", frameRate="60", backgroundColor="#4a4137")]
public class ApplicationMapMobile extends Sprite {
    public function ApplicationMapMobile() {
        if (this.stage) {
            this.stage.scaleMode = StageScaleMode.NO_SCALE;
            this.stage.align = StageAlign.TOP_LEFT;
        }
        this.mouseEnabled = this.mouseChildren = false;
        this.loaderInfo.addEventListener(Event.COMPLETE, loaderInfo_completeHandler);
    }

    private var _starling:Starling;
    private var stats:Stats;

    private function loaderInfo_completeHandler(event:Event):void {
        Starling.handleLostContext = true;
        Starling.multitouchEnabled = true;
        this._starling = new Starling(DemoMain, this.stage);
        this._starling.enableErrorChecking = false;
        this._starling.start();

        this.stage.addEventListener(Event.RESIZE, stage_resizeHandler, false, int.MAX_VALUE, true);
        this.stage.addEventListener(Event.DEACTIVATE, stage_deactivateHandler, false, 0, true);

        stats = new Stats;
        stats.x = stage.stageWidth - 70;
        addChild(stats);
    }

    private function stage_resizeHandler(event:Event):void {
        this._starling.stage.stageWidth = this.stage.stageWidth;
        this._starling.stage.stageHeight = this.stage.stageHeight;

        const viewPort:Rectangle = this._starling.viewPort;
        viewPort.width = this.stage.stageWidth;
        viewPort.height = this.stage.stageHeight;
        try {
            this._starling.viewPort = viewPort;
        }
        catch (error:Error) {
        }

        if (stats)
            stats.x = stage.stageWidth - 70;
    }

    private function stage_deactivateHandler(event:Event):void {
        this._starling.stop();
        this.stage.addEventListener(Event.ACTIVATE, stage_activateHandler, false, 0, true);
    }

    private function stage_activateHandler(event:Event):void {
        this.stage.removeEventListener(Event.ACTIVATE, stage_activateHandler);
        this._starling.start();
    }
}
}