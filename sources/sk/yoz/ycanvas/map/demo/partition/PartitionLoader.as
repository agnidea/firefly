package sk.yoz.ycanvas.map.demo.partition {
import flash.display.Loader;
import flash.net.URLRequest;
import flash.system.LoaderContext;

import sk.yoz.net.LoaderOptimizer;
import sk.yoz.ycanvas.map.partitions.IPartitionLoader;

public class PartitionLoader extends LoaderOptimizer implements IPartitionLoader {
    override public function load(request:URLRequest, context:LoaderContext):Loader {
//            trace(request.url,context.parameters);
        return super.load(request, context);
    }

    public function disposeLoader(loader:Loader):void {
        release(loader);
    }

}
}