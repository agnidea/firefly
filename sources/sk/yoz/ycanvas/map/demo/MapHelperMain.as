package sk.yoz.ycanvas.map.demo {
import flash.ui.Mouse;

import ru.agnidea.firefly.tablet.gis.layers.ObjectLayer;

import sk.yoz.utils.GeoUtils;
import sk.yoz.ycanvas.map.YCanvasMap;
import sk.yoz.ycanvas.map.demo.mock.Maps;
import sk.yoz.ycanvas.map.demo.partition.CustomPartitionFactory;
import sk.yoz.ycanvas.map.demo.partition.PartitionLoader;
import sk.yoz.ycanvas.map.display.MapLayer;
import sk.yoz.ycanvas.map.display.StrokeLayer;
import sk.yoz.ycanvas.map.events.CanvasEvent;
import sk.yoz.ycanvas.map.layers.LayerFactory;
import sk.yoz.ycanvas.map.managers.AbstractTransformationManager;
import sk.yoz.ycanvas.map.managers.MouseTransformationManager;
import sk.yoz.ycanvas.map.valueObjects.Limit;
import sk.yoz.ycanvas.map.valueObjects.MapConfig;
import sk.yoz.ycanvas.map.valueObjects.Transformation;

import starling.core.Starling;
import starling.display.Image;

/**
 * Provides functionality for the main map.
 */
public class MapHelperMain {
    public function MapHelperMain(partitionLoader:PartitionLoader) {
        var transformation:Transformation = new Transformation;
        transformation.centerX = GeoUtils.lon2x(7.75);
        transformation.centerY = GeoUtils.lat2y(45.53);
        transformation.rotation = 0;
        transformation.scale = 1 / 4096;

        var limit:Limit = new Limit;
        limit.minScale = 1;
        limit.maxScale = 1 / 65536;
        limit.minCenterX = 0;
        limit.maxCenterX = GeoUtils.lon2x(180);
        limit.minCenterY = GeoUtils.lat2y(85);
        limit.maxCenterY = GeoUtils.lat2y(-85);

        var config:MapConfig = Maps.ARCGIS_IMAGERY;

        map = new YCanvasMap(config, transformation, 256);

        //Lets customize partition factory so it creates CustomPartition
        // capable of handling bing maps
        map.partitionFactory = new CustomPartitionFactory(config, map, partitionLoader);
        map.layerFactory = new LayerFactory(config, map.partitionFactory);

        map.addEventListener(CanvasEvent.TRANSFORMATION_FINISHED, onMapTransformationFinished);
        transformationManager = Mouse.supportsCursor && !Starling.multitouchEnabled
                ? new MouseTransformationManager(map, limit)
                : new MouseTransformationManager(map, limit);
        //TouchTransformationManager


        polygonLayer = new MapLayer;
//            polygonLayer.addEventListener(TouchEvent.TOUCH, onPolygonLayerTouch);
        map.addMapLayer(polygonLayer);

        strokeLayer = new StrokeLayer;
        strokeLayer.autoUpdateThickness = false;
//            strokeLayer.addEventListener(TouchEvent.TOUCH, onStrokeLayerTouch);
        map.addMapLayer(strokeLayer);

        markerLayer = new ObjectLayer;
//            objectsLayer.addEventListener(TouchEvent.TOUCH, onMarkerLayerTouch);
        map.addMapLayer(markerLayer);
    }

    public var map:YCanvasMap;
    public var polygonLayer:MapLayer;
    public var strokeLayer:StrokeLayer;
    public var markerLayer:ObjectLayer;
    public var transformationManager:AbstractTransformationManager;

    public function addMarkerAt(x:Number, y:Number):void {
        var marker:Image = new Image(Assets.MARKER_GREEN_TEXTURE);
        marker.x = x;
        marker.y = y;
        marker.pivotX = Assets.MARKER_GREEN_TEXTURE.width / 2;
        marker.pivotY = Assets.MARKER_GREEN_TEXTURE.height;
        markerLayer.add(marker);
    }

    public function dispose():void {
        map.removeEventListener(CanvasEvent.TRANSFORMATION_FINISHED, onMapTransformationFinished);
        map.dispose();
        map = null;

        transformationManager.dispose();
        transformationManager = null;

        polygonLayer = null;
        strokeLayer = null;
        markerLayer = null;
    }


    private function onMapTransformationFinished(event:CanvasEvent):void {
        if (!strokeLayer.autoUpdateThickness)
            strokeLayer.updateThickness();
    }
}
}