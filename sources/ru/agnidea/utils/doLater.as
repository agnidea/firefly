package ru.agnidea.utils {

public function doLater(method:Function, delay:int = 1, useMiliseconds:Boolean = false):int {
    if (useMiliseconds) delay = delay / (1000 / Fps.rate );
    return FTimer.addTask(method, delay);
}
}