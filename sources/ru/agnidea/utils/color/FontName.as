package ru.agnidea.utils.color {
public class FontName {
    public static const ROBOTO:String = "Roboto";
    public static const ROBOTO_MEDIUM:String = "Roboto-Medium";
    public static const ROBOTO_LIGHT:String = "Roboto-Light";
    public static const ROBOTO_THIN:String = "Roboto-Thin";

    public static const ROBOTO_MX:String = "Roboto-MX";
    public static const ROBOTO_LIGHT_MX:String = "Roboto-Light-MX";
    public static const ROBOTO_MEDIUM_MX:String = "Roboto-Medium-MX";
    public static const ROBOTO_THIN_MX:String = "Roboto-Thin-MX";
}
}
