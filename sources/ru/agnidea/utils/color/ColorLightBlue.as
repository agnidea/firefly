/**
 * Created by flower on 27.11.14.
 */
package ru.agnidea.utils.color {
public class ColorLightBlue {
    public const l50:int = 0xE1F5FE;
    public const l100:int = 0xB3E5FC;
    public const l200:int = 0x81D4FA;
    public const l300:int = 0x4FC3F7;
    public const l400:int = 0x29B6F6;
    public const l500:int = 0x03A9F4;
    public const l600:int = 0x039BE5;
    public const l700:int = 0x0288D1;
    public const l800:int = 0x0277BD;
    public const l900:int = 0x01579B;
    public const a100:int = 0x80D8FF;
    public const a200:int = 0x40C4FF;
    public const a400:int = 0x00B0FF;
    public const a700:int = 0x0091EA;

    public function ColorLightBlue() {
    }
}
}
