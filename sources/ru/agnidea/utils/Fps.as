package ru.agnidea.utils {
import mx.core.FlexGlobals;

import ru.agnidea.firefly.Core;

public class Fps {

    public static function get rate():int {
        if (!FlexGlobals.topLevelApplication.frameRate) FlexGlobals.topLevelApplication.frameRate = 60;
        return FlexGlobals.topLevelApplication.frameRate;
    }

    public static function setRate(value:int):void {
        Core.view.stage.frameRate = value;
        FlexGlobals.topLevelApplication.frameRate = value;
    }

    public function Fps() {
    }

}
}
