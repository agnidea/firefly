package ru.agnidea.utils {
import flash.system.Capabilities;

public function adaptFontSizeToCurDPI(fontSize:uint):uint {
    // эталонная dpi - 264 так как размер шрифта по умолчанию указываем для ipad air retina


    if (SystemInfo.mobile) {
        return uint(Capabilities.screenDPI * fontSize / 264);
    } else {
        return uint(fontSize / 1.1);
    }
}
}
