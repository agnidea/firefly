/**
 * Created created in fire. With love.
 */
package ru.agnidea.utils {
import flash.system.Capabilities;

public function cmToPx(cm:Number):int {

    if (SystemInfo.mobile) {
        var inch:Number = cm * 0.39370;
        return int(Capabilities.screenDPI * inch);
    } else {
        return int(cm * 55);
    }
}
}
