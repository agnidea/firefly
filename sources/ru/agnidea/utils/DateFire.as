/**
 * Created by flower on 24.11.14.
 */
package ru.agnidea.utils {
import spark.formatters.DateTimeFormatter;

public class DateFire {
    private static var _dtFormater:DateTimeFormatter;

    private static function get dtFormater():DateTimeFormatter {
        if (_dtFormater)
            return _dtFormater;
        _dtFormater = new DateTimeFormatter();
        _dtFormater.setStyle('locale', 'ru_RU');
        return _dtFormater;
    }


    public static function dateRu(startDate:Date):String {
        dtFormater.timeStyle = 'none';
        dtFormater.dateStyle = 'medium';
        return dtFormater.format(startDate);
    }

    public static function time(date:Date):String {
        dtFormater.timeStyle = 'medium';
        dtFormater.dateStyle = 'none';
        return dtFormater.format(date);

    }

    public function DateFire() {
    }
}
}
