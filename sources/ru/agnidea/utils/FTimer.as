package ru.agnidea.utils {
import flash.events.TimerEvent;
import flash.utils.Timer;

public class FTimer {
    private static var instance:FTimer = new FTimer();

    public static function addTask(testFunc:Function, delay:int):int {
        return instance.add(testFunc, delay);
    }

    public static function removeTask(index:int):void {
        instance.remove(index);
    }

    public static function updateTask(testFunc:Function, delay:int):void {
        instance.update(testFunc, delay);
    }

    public function FTimer() {
        functions = {};
        indexes = new Vector.<int>();
        runDelays = new Vector.<int>();
    }

    private var timer:Timer;
    private var index:int = 0;
    private var functions:Object;
    private var indexes:Vector.<int>;
    private var runDelays:Vector.<int>;
    private var totalCount:uint = 0;

    public function update(testFunc:Function, delay:int):void {
        var i:int = indexes.length;
        var find:Boolean = false;
        while (i--) {
            var n:int = indexes[i];
            if (functions[n] == testFunc) {
                runDelays[i] = (delay / 50 + timer.currentCount + 1);
                find = true;
            }
        }
        if (!find) add(testFunc, delay);
    }

//    public function printObj(obj:Object):String {
//        var res:String = "";
//        for (var nameV:String in obj) {
//            res += nameV + ", ";
//        }
//        return res;
//    }

    private function add(func:Function, delay:int):int {
        if (!timer) {
            timer = new Timer(50);
            timer.addEventListener(TimerEvent.TIMER, timerHandler);
            timer.start();
            functions = {};
            indexes = new Vector.<int>();
            runDelays = new Vector.<int>();
        }
        if (index == int.MAX_VALUE) resetIndexes();
        index++;
        functions[index] = func;
        runDelays.push(delay / 50 + timer.currentCount + 1);
        indexes.push(index);
        return index;
    }

    private function resetIndexes():void {
        var i:int = indexes.length;
        var newDelays:Vector.<int> = new Vector.<int>();
        var newIndexes:Vector.<int> = new Vector.<int>();
        while (i--) {
            newDelays.push(runDelays[i]);
            newIndexes.push(indexes[i]);
        }
        indexes = newIndexes;
        runDelays = newDelays;
        index = 0;
    }

    private function timerHandler(e:*):void {
        totalCount++;
        var i:int = indexes.length;
        while (i--) {
            if (totalCount > runDelays[i]) {
                var n:int = indexes[i];

                if (functions[n] is Function)
                    functions[n]();
                delete  functions[n];
                runDelays.splice(i, 1);
                indexes.splice(i, 1);
            }
        }
        timer.start();
    }

    private function remove(index:int):void {
        var i:int = indexes.length;
        while (i--) {
            if (indexes[i] == index) {
                delete  functions[indexes[i]];
                runDelays.splice(i, 1);
                indexes.splice(i, 1);
                //SStepuro: мне кажется цель достигнута можно уходить из цикла, или нет?
                break;
            }
        }

    }

}
}
