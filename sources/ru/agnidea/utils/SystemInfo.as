/**
 * Created by flower on 13.12.14.
 */
package ru.agnidea.utils {
import flash.system.Capabilities;

public class SystemInfo {
    public static function get mobile():Boolean {
        if (Capabilities.os.indexOf("Mac") >= 0 || Capabilities.os.indexOf("Win") >= 0) {
            return false
        } else {
            return true;
        }

    }

    public function SystemInfo() {
    }
}
}
