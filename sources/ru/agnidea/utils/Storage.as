/**
 * Created by flower on 14.11.14.
 */
package ru.agnidea.utils {
import flash.display.BitmapData;
import flash.events.Event;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.net.SharedObject;
import flash.utils.ByteArray;

public class Storage {
    private static var pattern:RegExp = /(\/|:)/g;

    public static function writeBitmap(bitmapData:BitmapData, url:String):void {

        var fileName:String = url.replace(pattern, "");
        var file:File = File.applicationStorageDirectory.resolvePath(fileName);
        if (!file.exists) {
            var stream:FileStream = new FileStream();
            var bytes:ByteArray = new ByteArray();
            bytes.writeUnsignedInt(bitmapData.width);
            bytes.writeBytes(bitmapData.getPixels(bitmapData.rect));
            stream.openAsync(file, FileMode.WRITE);
            stream.writeBytes(bytes); // value being the clipboard content
            stream.close();
        }
    }

    public static function readBitmap(url:String, onComplete):Boolean {
        var fileName:String = url.replace(pattern, "");
        var file:File = File.applicationStorageDirectory.resolvePath(fileName);
        if (file.exists) {
            var stream:FileStream = new FileStream();
            stream.addEventListener(Event.COMPLETE, completeHandler);
            stream.openAsync(file, FileMode.READ);

            function completeHandler(event:Event):void {
                var ba:ByteArray = new ByteArray();
                stream.readBytes(ba);
                stream.close();

                var width:int = ba.readUnsignedInt();
                var height:int = ((ba.length - 4) / 4) / width;
                var bmd:BitmapData = new BitmapData(width, height, true, 0);
                bmd.setPixels(bmd.rect, ba);
                onComplete(bmd);
            }

            function getBitmapData(e:Event):void {
                onComplete(e.target.content);
            }

            return true;
        } else {
            return false;
        }
    }

    public function Storage() {
        so = SharedObject.getLocal("main");


    }

    public var so:SharedObject;


}
}
