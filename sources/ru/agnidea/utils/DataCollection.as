package ru.agnidea.utils {
import flash.net.SharedObject;
import flash.utils.Dictionary;
import flash.utils.Proxy;
import flash.utils.flash_proxy;

import mx.collections.ArrayList;

use namespace flash_proxy;

public class DataCollection extends Proxy {
    public function DataCollection(identityFieldName:String, className:String) {
        indexes = new Vector.<String>();
        hashMap = new Dictionary();
        source = new Object();
        this.className = className;
        this.identityFieldName = identityFieldName;
    }

    public var className:String;
    private var identityFieldName:String;
    private var indexes:Vector.<String>;
    private var hashMap:Dictionary;
    private var source:Object;
    private var id:String;
    private var i:int;
    private var ar:Array;

    public function get length():int {
        return indexes.length;
    }

    private var _so:SharedObject;

    public function get so():SharedObject {
        if (!_so) _so = SharedObject.getLocal(className);
        return _so;
    }

    /**
     *  добавлена возможность обхода по циклу
     *  for each (var item1:* in DataCollection) {
     *  и упрощён вызов по id DataCollection[id]
     */
    override flash_proxy function getProperty(id:*):* {
        return source[id];
    }

    override flash_proxy function nextValue(index:int):* {
        var o:Object = source[indexes[index - 1]];
        return o;
    }

    override flash_proxy function nextNameIndex(index:int):int {
        if (index < indexes.length) return index + 1;
        return 0;
    }

    public function addItemAt(item:Object, index:int):void {
        id = item[identityFieldName];
        if (source[id] == item) return;
        if (source[id])
            throw new Error("У двух разных элементов в коллекции не может быть одинакового id");
        indexes.splice(index, 0, id);
        source[id] = item;
        hashMap[item] = id;
    }

    public function getItemAt(index:int):Object {
        return source[indexes[index]];
    }

    public function removeItemAt(index:int):Object {
        id = indexes[index];
        indexes.splice(index, 1);
        delete hashMap[source[id]];
        delete source[id];
        return true;
    }

    public function removeItem(item:Object):Object {
        id = item[identityFieldName];
        indexes.splice(getIndexById(id), 1);
        delete source[id];
        delete hashMap[item];
        return true;
    }

    public function getIndexById(id:String):int {
        for (i = 0; i < indexes.length; i++) {
            if (indexes[i] == id) return i;
        }
        return -1;
    }

    public function getItemById(id:String):Object {
        return source[id];
    }

    public function removeItemById(id:String):Object {
        indexes.splice(getIndexById(id), 1);
        delete hashMap[source[id]];
        delete source[id];
        return true;
    }

    public function add(item:Object):void {
        id = item[identityFieldName];
        if (source[id] == item) return;
//        if (source[id])
//            throw new Error("У двух разных элементов в коллекции не может быть одинакового id");
        source[id] = item;
        hashMap[item] = id;
        indexes.push(id);
    }

    public function toArray():Array {
        ar = [];
        i = indexes.length;
        while (i--) {
            ar.push(source[indexes[i]]);
        }
        return ar;
    }

    public function toArrayList():ArrayList {
        ar = [];
        i = indexes.length;
        while (i--) {
            ar.push(source[indexes[i]]);
        }
        return new ArrayList(ar);
    }

    public function clear():void {
        indexes = new Vector.<String>();
        hashMap = new Dictionary();
        source = new Object();
    }

    public function store():void {
        i = indexes.length;
        so.clear();
        while (i--) {
            so.data[i] = source[indexes[i]];
        }
        so.flush();
    }

    public function read():void {
        for (var s:String in so.data) {
            addItemAt(so.data[s], int(i));
        }
    }
}
}