/**
 * Created by flower on 23.11.14.
 */
package ru.agnidea.firefly.core.data {
import ru.agnidea.firefly.core.vo.PerfectObject;

public class UserObjects {

    public static var granted:PerfectObject;

    public function UserObjects() {
        granted = new PerfectObject(PerfectObject.COLLECTION);
        allowedEID["770353451047005960"] = true;
        allowedEID["770353451048044992"] = true;
        allowedEID["770353451048051203"] = true;
        allowedEID["770353451047181910"] = true;
        allowedEID["770353451046992325"] = true;
        allowedEID["770353451047189665"] = true;
    }
    private var allowedEID:Object = new Object();

    public function gmsHandler(gmsObjects:PerfectObject):void {
        for each (var object:PerfectObject in gmsObjects.objects) {
            if (allowedEID[object.uid]) {
                var storedPerfetc:PerfectObject = granted.objectsDic[object.uid];
                if (storedPerfetc) {
                    granted.updateObject(object);
                }
                granted.add(object);
            }
        }
    }

    public function getByHardname(hardName:String):PerfectObject {
        for each (var object:PerfectObject in granted.objects) {
            if (object.uid == hardName)
                return object;
        }
        return null;
    }

    public function add(perfectObject:PerfectObject):void {
        if (!granted.updateObject(perfectObject)) {
            granted.add(perfectObject);
        }
    }
}
}
