/**
 * Created by flower on 14.12.14.
 */
package ru.agnidea.firefly.core.data {
import ru.agnidea.utils.DataCollection;

public class Classifier {

    public function Classifier() {

    }

    public var className:String;
    public var name:String;
    public var params:Object;
    public var color:int;
    public var childrens:DataCollection;
}
}
