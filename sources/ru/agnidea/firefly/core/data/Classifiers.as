/**
 * Created by flower on 14.12.14.
 */
package ru.agnidea.firefly.core.data {
public class Classifiers {
    public static var allList:Array = new Array();

    public function Classifiers() {
        var classifier:Classifier;
        classifier = new Classifier();
        classifier.className = "CultureVO";
        classifier.name = "Культуры СельХоз";
        classifier.params = {};
        allList.push(classifier);

        classifier = new Classifier();
        classifier.className = "TSCategoryVO";
        classifier.name = "Категории ТС";
        classifier.params = {};
        allList.push(classifier);

        classifier = new Classifier();
        classifier.className = "ObjectInfoVO";
        classifier.name = "Подвижные объекты";
        classifier.params = {};
        allList.push(classifier);

        classifier = new Classifier();
        classifier.className = "xxx";
        classifier.name = "Виды маршрутов";
        classifier.params = {};
        allList.push(classifier);


    }
}
}
