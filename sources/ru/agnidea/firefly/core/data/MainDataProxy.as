/**
 * Created by flower on 23.11.14.
 */
package ru.agnidea.firefly.core.data {
import com.gamua.flox.Player;
import com.gamua.flox.Query;

import org.mvcexpress.mvc.Proxy;

import ru.agnidea.firefly.core.services.GmsService;
import ru.agnidea.firefly.core.services.KmlService;
import ru.agnidea.firefly.core.vo.CultureVO;
import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.firefly.core.vo.TSCategoryVO;
import ru.agnidea.firefly.tablet.events.DataEvents;
import ru.agnidea.firefly.tablet.events.NetworkEvents;
import ru.agnidea.firefly.tablet.events.PerfectEvents;

public class MainDataProxy extends Proxy {
    public function MainDataProxy() {
    }


    public var catalog:CatalogStore;
    public var gms:GmsService;
    public var kml:KmlService;
//    public var userObjects:UserObjects

    override protected function onRegister():void {
        gms = new GmsService();
        kml = new KmlService();
        catalog = new CatalogStore();
//        userObjects = new UserObjects();
        gms.onObjectsLoaded = onGmsLoad;
        gms.onError = onGmsError;
        kml.onPlacesLoaded = onKmlLoad;
    }

    public function updateData():void {

        var query:Query = new Query(PerfectObject, "ownerId == ?", Player.current.id);

        query.find(
                function onComplete(ar:Array):void {
                    var poly:Boolean = false;
                    var player:Boolean = false;
                    var perfect:PerfectObject;
                    for each (var perfectObject:PerfectObject in ar) {
                        if (perfectObject.mainType == PerfectObject.POLY || perfectObject.mainType == PerfectObject.LINE) {
                            perfect = catalog.geoObjects[perfectObject.uid];
                            if (perfect) {
                                perfect.update(perfectObject);
                            } else {
                                catalog.geoObjects.add(perfectObject);
                            }
                            poly = true;
                        } else if (perfectObject.mainType == PerfectObject.PLAYER) {
                            perfect = catalog.movingObjects[perfectObject.uid];
                            if (perfect) {
                                perfect.update(perfectObject);
                            } else {
                                catalog.movingObjects.add(perfectObject);
                            }
                            player = true;
                        }
                    }
                    if (poly) {
                        sendMessage(PerfectEvents.PLACES_LOADED, catalog.geoObjects);
//                        catalog.geoObjects.store();
                    }
                    if (player) {
                        sendMessage(PerfectEvents.REFRESH_PLAYERS);
//                        catalog.movingObjects.store();
                    }
                },
                function onError(error:String):void {
                    trace(error);
                }
        );
    }

    public function loadCatalogs():void {
        var query:Query = new Query(CultureVO);
        query.find(
                function onComplete(ar:Array):void {
                    catalog.agroCultures.clear();
                    for each (var o:CultureVO in ar) {
                        catalog.agroCultures.add(o);
                    }
                },
                function onError(error:String):void {
                    trace("FLOX ERROR: CultureVO:", error);
                }
        );

        var query:Query = new Query(TSCategoryVO);
        query.find(
                function onComplete(ar:Array):void {
                    catalog.tsCategories.clear();
                    for each (var o:TSCategoryVO in ar) {
                        catalog.tsCategories.add(o);
                    }
                },
                function onError(error:String):void {
                    trace("FLOX ERROR: TSCategoryVO:", error);
                }
        );
    }

    private function onGmsError(data:String):void {
        sendMessage(NetworkEvents.GMS_ERROR, data);
    }

    private function onGmsLoad():void {
        sendMessage(NetworkEvents.GMS_UPDATE);
    }

    private function onKmlLoad(perfect:PerfectObject):void {
        catalog.geoObjects.add(perfect);
        sendMessage(DataEvents.CATALOG_GEO_UPDATE);
    }
}
}
