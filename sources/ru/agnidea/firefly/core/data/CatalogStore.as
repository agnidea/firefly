/**
 * Created by flower on 24.11.14.
 */
package ru.agnidea.firefly.core.data {
import ru.agnidea.utils.DataCollection;

public class CatalogStore {
    public function CatalogStore() {


        var classifier:Classifier;
        classifier = new Classifier();
        classifier.className = "CultureVO";
        classifier.name = "(alpha) категории";
        classifier.params = {};
        classifier.childrens = agroCultures;
        classifiers.add(classifier);

        classifier = new Classifier();
        classifier.className = "TSCategoryVO";
        classifier.name = "(alpha) Категории ТС";
        classifier.params = {};
        classifier.childrens = tsCategories;
        classifiers.add(classifier);

        classifier = new Classifier();
        classifier.className = "ObjectInfoVO";
        classifier.name = "Подвижные объекты";
        classifier.params = {};
        classifier.childrens = movingObjects;
        classifiers.add(classifier);

        classifier = new Classifier();
        classifier.className = "xxx";
        classifier.name = "Геометрия";
        classifier.params = {};
        classifier.childrens = geoObjects;
        classifiers.add(classifier);

//        classifier = new Classifier();
//        classifier.className = "TracksVO";
//        classifier.name = "Маршруты";
//        classifier.params = {};
//        classifier.childrens = geoObjects;
//        classifiers.add(classifier);


//        geoObjects.read();
//        movingObjects.read();
    }

    public var classifiers:DataCollection = new DataCollection("className", "Classifier");
    public var agroCultures:DataCollection = new DataCollection("id", "CultureVO");
    public var tsCategories:DataCollection = new DataCollection("id", "TSCategoryVO");
    public var movingObjects:DataCollection = new DataCollection("uid", "movingObjects");
    public var geoObjects:DataCollection = new DataCollection("uid", "geoObjects");


}
}