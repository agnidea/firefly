package ru.agnidea.firefly.core {
import flash.events.GeolocationEvent;
import flash.geom.Point;
import flash.sensors.Geolocation;

import org.mvcexpress.mvc.Proxy;

import ru.agnidea.firefly.tablet.events.NetworkEvents;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class GPSProxy extends Proxy {

    public function GPSProxy() {
    }

    public var p:Point;
    private var gpsIsSupported:Boolean = false;


//    private var map:TileMap;
//    public function FireGPS(map:TileMap) {
//        this.map = map;
//    }
    private var gpsTried:Boolean = true;
    private var gps:Geolocation;

    override protected function onRegister():void {
        initGPS();
    }

    override protected function onRemove():void {
    }


    /*
     function to create the GPS object
     */

    public function initGPS():void {
        gpsTried = true;

        if (Geolocation.isSupported) {
            gpsIsSupported = true;
            if (!gps) gps = new Geolocation();
            setGpsLatLng();
        }
    }


    public function setGpsLatLng():void {
        if (!gpsIsSupported) return;

        if (!gps.hasEventListener(GeolocationEvent.UPDATE)) {
            gps.addEventListener(GeolocationEvent.UPDATE, onGeoUpdate, false, 0, true);
        }
    }


    private function onGeoUpdate(e:GeolocationEvent):void {
        if (e.horizontalAccuracy < 100) {
            p = new Point(Number(e.latitude.toFixed(6)), Number(e.longitude.toFixed(6)));
            gps.removeEventListener(GeolocationEvent.UPDATE, onGeoUpdate);
            sendMessage(NetworkEvents.GPS_UPDTATE, p);
        }

    }

}
}