/**
 * Created by flower on 25.10.14.
 */
package ru.agnidea.firefly.core {
import flash.geom.Point;
import flash.net.SharedObject;

import org.mvcexpress.mvc.Proxy;

import ru.agnidea.firefly.core.vo.SettingsData;

import sk.yoz.ycanvas.map.valueObjects.MapConfig;

public class SettingsProxy extends Proxy {

    public function SettingsProxy() {
    }

    public var data:SettingsData;
    private var so:SharedObject;

    override protected function onRegister():void {
        data = new SettingsData();
        so = SharedObject.getLocal("settings");
        restore();
    }

    override protected function onRemove():void {
    }


    public function restore():void {
        data = new SettingsData();
        for (var param:String in so.data) {
            if (so.data[param]) {
                data[param] = so.data[param];
            }
        }
    }




    public function saveLocationAndZoom(center:Point, scale:Number):void {
        data.startYX = center;
        so.data["startX"] = data.startX;
        so.data["startY"] = data.startY;
        so.data["startZoom"] = data.startZoom = scale;
        so.flush();
    }

    public function saveMapType(mapConfig:MapConfig):void {
        so.data["currentMapType"] = data.currentMapType = data.mapTypeToString(mapConfig);
        so.flush();
    }

    public function saveLogin(login:String):void {
        so.data["login"] = data.login = login;
        so.flush();
    }

    public function savePass(pass:String):void {
        so.data["pass"] = data.pass = pass;
        so.flush();
    }

    public function saveHidePass(value:Boolean):void {
        so.data["hidepass"] = data.hidepass = value;
        so.flush();
    }

    public function saveDeviceID(deviceID:String):void {
        so.data["deviceID"] = data.deviceID = deviceID;
        so.flush();
    }


}
}
