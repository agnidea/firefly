/**
 * Created by flower on 23.11.14.
 */
package ru.agnidea.firefly.core {
import ru.agnidea.firefly.core.vo.PerfectObject;

public class AppState {
    public static var currentPerfect:PerfectObject;
    public static var hideGis:Boolean = false;
    public static var infoCardState:String = "map";
    public static var unreadMessges:int = 0;

    public function AppState() {
    }
}
}
