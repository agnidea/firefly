/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.core.vo {
import flash.net.NetworkInfo;
import flash.net.NetworkInterface;
import flash.system.Capabilities;

public class User {
    public static var login:String = "";
    public static var pass:String = "";
    public static var deviceID:String;
    private static var _udid:String;

    private static var _info:String;

    public static function get info():String {
        if (!_info)
            return Capabilities.os;
        else
            return _info;

    }

    public static function get mac():String {
        if (_udid) return _udid;

        if (NetworkInfo.isSupported) {
            trace("network information is supported");
        }
        var network:NetworkInfo = NetworkInfo.networkInfo;
        for each (var object:NetworkInterface in network.findInterfaces()) {
            if (object.hardwareAddress) {
                _udid = object.hardwareAddress
                return _udid;
            }
        }
        return Capabilities.os;
    }

    public static function ipAddress():void {
        var networkInfo = NetworkInfo.networkInfo;
        var interfaces = networkInfo.findInterfaces();
        var interfaceObj:Object;
        var address:Object;

        //Get available interfaces
        for (var i = 0; i < interfaces.length; i++) {
            interfaceObj = interfaces[i];
            for (var j = 0; j < interfaceObj.addresses.length; j++) {
                address = interfaceObj.addresses[j];
                trace(address.address + "\n");
            }
        }
    }

    public function User() {
    }

}
}
