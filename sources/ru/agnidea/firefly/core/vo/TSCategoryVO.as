/**
 * Created by flower on 27.11.14.
 */
package ru.agnidea.firefly.core.vo {
import com.gamua.flox.Access;
import com.gamua.flox.Entity;

[RemoteClass(alias="ru.agnidea.firefly.core.vo.TSCategoryVO")]
public class TSCategoryVO extends Entity {
    public function TSCategoryVO() {

        publicAccess = Access.READ_WRITE;
    }

    public var name:String = "";
    public var color:int = 0xFF00FF;


    public function get label():String {
        return name;
    }
}
}
