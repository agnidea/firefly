/**
 * Created by flower on 25.10.14.
 */
package ru.agnidea.firefly.core.vo {
import flash.geom.Point;

import sk.yoz.ycanvas.map.demo.mock.Maps;
import sk.yoz.ycanvas.map.valueObjects.MapConfig;

public dynamic class SettingsData {
    public function SettingsData() {
    }

    public var login:String = "";
    public var pass:String = "";
    public var hidepass:Boolean = true;
    public var deviceID:String;

    public var currentMapType:String;
    public var startY:Number = 1.9344729803166036E7;
    public var startX:Number = 4.753573388232822E7;
    public var startZoom:Number = 5.4445364061836746E-5;
    public var autoSaveLocation:Boolean = true;


    public function get startYX():Point {
        if (startX && startY) return new Point(startX, startY);
        else return null;
    }

    public function set startYX(p:Point):void {
        startX = p.x;
        startY = p.y;
    }


    public function getCurrentMapType():MapConfig {
        if (currentMapType)
            switch (currentMapType) {
                case "rb1":
                    return Maps.ARCGIS_IMAGERY;
                case "rb2":
                    return Maps.BINGMAPS_IMAGERY;
                case "rb3":
                    return Maps.ARCGIS_NATIONAL_GEOGRAPHIC;
                case "rb4":
                    return Maps.MAPQUEST;
                case "rb5":
                    return Maps.OSM;
                case "rb6":
                    return Maps.MAPBOX;
            }
        return Maps.OSM;
    }

    public function mapTypeToString(mapConfig:MapConfig):String {
        switch (mapConfig) {
            case Maps.ARCGIS_IMAGERY :
                return "rb1";
            case Maps.BINGMAPS_IMAGERY :
                return "rb2";
            case Maps.ARCGIS_NATIONAL_GEOGRAPHIC :
                return "rb3";
            case Maps.MAPQUEST :
                return "rb4";
            case Maps.OSM :
                return "rb5";
            case Maps.MAPBOX :
                return "rb6";
        }
        return null;
    }
}
}
