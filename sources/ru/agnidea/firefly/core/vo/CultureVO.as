/**
 * Created by flower on 24.11.14.
 */
package ru.agnidea.firefly.core.vo {
import com.gamua.flox.Access;
import com.gamua.flox.Entity;

[RemoteClass(alias="ru.agnidea.firefly.core.vo.CultureVO")]
public class CultureVO extends Entity {
    public function CultureVO() {
        publicAccess = Access.READ_WRITE;
    }

    public var name:String = "";
    public var color:int = 0xFF00FF;

    public function get label():String {
        return name;
    }
}
}