/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.core.vo {

import com.gamua.flox.Access;
import com.gamua.flox.Entity;
import com.mapquest.LatLng;
import com.mapquest.tilemap.overlays.LineOverlay;
import com.mapquest.tilemap.overlays.PolygonOverlay;
import com.mapquest.tilemap.pois.Poi;

import flash.geom.Point;

import ru.agnidea.firefly.Core;

import sk.yoz.utils.GeoUtils;

[RemoteClass(alias="ru.agnidea.firefly.core.vo.PerfectObject")]
public class PerfectObject extends Entity {

    public static const COLLECTION:String = "collection";
    public static const PLACE:String = "place";
    public static const PLAYER:String = "player";
    public static const SOUL:String = "soul";
    public static const POLY:String = "polygon";
    public static const LINE:String = "line";

//    private static var dict:Object = {};

    public function PerfectObject(type:String = "none") {
        this.mainType = type;
        publicAccess = Access.READ;
    }

//    public static function gethard(hardName:String):PerfectObject {
//        return dict[hardName];
//    }
    [NonSerialized]
    public var geoUpdate:Function;
    [NonSerialized]
    public var data:Object = new Object();

    public var startDate:Date;
    public var endDate:Date;
    public var visible:Boolean = false;

    [NonSerialized]
    public var childrens:Array = [];
    [NonSerialized]
    public var objects:Array = [];
//    [NonSerialized]
    public var objectsDic:Object = new Object();
    public var name:String;
    public var uid:String;
    public var mainType:String;
    public var date:Date;
    public var sizeGa:Number = 10;
    public var cultureID:int = 0;
    public var transportCatID:int = 12;
    public var transportGosNumber:String;
    public var color:int;
    public var storeCoords:Array;
    public var speed:Number;
    public var angle:Number;
    public var updateTime:String;
    public var saved:Boolean = true;
    private var updateData:String;
//    public var id:String;
    private var _p:Point;

    public var _center:Object;

    public function get center():Point {
        if (!_p && _center) _p = new Point(_center.x, _center.y);
        return _p;
    }

    [NonSerialized]
    private var _geoVector:Vector.<Number>;

    public function get geoVector():Vector.<Number> {
        if (!storeCoords) return null;
        if (!_geoVector || _geoVector.length != storeCoords.length) {
            _geoVector = new Vector.<Number>();
            for (var i:int = 0; i < storeCoords.length; i++) {
                _geoVector.push(storeCoords[i]);
            }
        }
        return _geoVector;
    }

    [NonSerialized]
    public function get label():String {
        if (name)
            return name;
        return ""
    }

    [NonSerialized]
    public function get twoDay():Boolean {
        var d:Date = new Date();
        return (d.time - date.time < 172800000) && (d.time - date.time > 180000)
    }

    [NonSerialized]
    public function get oneYear():Boolean {
        var d:Date = new Date();
//        trace(updateData,updateTime);
        return (d.time - date.time < 31536000000)
    }

    [NonSerialized]
    public function get fiveMinutes():Boolean {
        var d:Date = new Date();
//        trace(updateData,updateTime);
//        trace(d);
//        trace(date);
//        trace(d.time-date.time,(d.time-date.time < 10800000));
        return (d.time - date.time < 180000)
    }

    [NonSerialized]
    public function get oneMount():Boolean {
        var d:Date = new Date();
//        trace(updateData,updateTime);
//        trace(d);
//        trace(date);
//        trace(d.time-date.time,(d.time-date.time < 2592000000));

        return (d.time - date.time < 2592000000) && (d.time - date.time > 172800000)
    }

    [NonSerialized]
    public function get state():String {
        if (oneMount) {
            return "lost"
        }
        if (twoDay) {
            return "days"
        }
        if (speed == 0) {
            return "parked"
        }
        return "drive"
    }

    [NonSerialized]
    public function get photo():int {
        if (mainType == POLY) {
            switch (cultureID) {
                case 1:
                    return 3;
                case 2:
                    return 6;
                case 3:
                    return 5;
                case 4:
                    return 4;
                case 5:
                    return 7;
            }
            return 8;
        } else if (mainType == PLAYER) {
            return transportCatID;
        } else if (mainType == SOUL) {
            return 13;
        }
        return 1;
    }

    override public function saveQueued():void {
        saved = true;
        super.saveQueued();
    }

    override public function save(onComplete:Function, onError:Function):void {
        saved = true;
        super.save(onComplete, onError);
    }

    public function addChildren(perfecObject:PerfectObject):void {
        childrens.push(perfecObject);
    }

    public function add(perfecObject:PerfectObject):void {
        objects.push(perfecObject);
        objectsDic[perfecObject.uid] = perfecObject;
    }

    [NonSerialized]
    public function initGeo(o:*):void {
        saved = false;
        if (o is Poi) {
            var poi:Poi = o as Poi;
            mainType = "poi";
            _center = _p = new Point(poi.latLng.lat, poi.latLng.lng);
            if (poi.key)
                name = poi.key;
            else
                name = poi.infoWindowTitleText;
        }
        else if (o is PolygonOverlay) {
            var polygon:PolygonOverlay = o as PolygonOverlay;
            storeCoords = [];//new Vector.<Number>();
            for each (var ll:LatLng in o.shapePoints) {
                storeCoords.push(GeoUtils.lon2x(ll.lng), GeoUtils.lat2y(ll.lat));
            }
            mainType = "polygon";
            _center = _p = new Point(polygon.boundingShapePoints.center.lat, polygon.boundingShapePoints.center.lng);
            name = polygon.key;

        } else if (o is LineOverlay) {
            var line:LineOverlay = o as LineOverlay;
            mainType = "line";
            _center = _p = new Point(line.shapePoints.center.lat, line.shapePoints.center.lng);
            storeCoords = [];
            for each (var ll:LatLng in line.shapePoints) {
                storeCoords.push(GeoUtils.lon2x(ll.lng), GeoUtils.lat2y(ll.lat));
            }
            name = line.key;
        }
        else if (o is PolygonOverlay) {

        }
        uid = name;
        id = User.login + "-" + mainType + "-" + Math.round(Math.random() * 100000);
    }

    [NonSerialized]
    public function initGMS(object:Object):void {
        if (!name) name = object.equip_id;
        uid = object.equip_id;
        id = User.login + "_" + uid;
        transportGosNumber = name;
//        dict[id] = this;
        _center = _p = new Point(adapt(object.gps_lon), adapt(object.gps_lat));


        angle = parseInt(object.gps_kurs);

        speed = parseFloat(object.gps_speed);
        updateTime = object.GPS_time;
        var newData:String = object.GPS_time;
        if (newData != updateData) {
            updateData = object.GPS_date;
            date = strToDate(updateData + " " + updateTime);
            if (geoUpdate != null) geoUpdate();
        } else {
            updateData = object.GPS_date;
            date = strToDate(updateData + " " + updateTime);
        }

    }

    [NonSerialized]
    public function strToDate(dtm:String) {
        var date:Date = new Date;
        var ar:Array = new Array();
        var st:String = new String();
        ar = dtm.split(" ");
        st = ar[0];
        ar = st.split("-");
        date.setUTCFullYear(ar[0]);
        date.setUTCMonth(int(ar[1]) - 1);
        date.setUTCDate(ar[2]);
        ar = dtm.split(" ");
        st = ar[1];
        ar = st.split(":");
        date.setUTCHours(ar[1]);
        date.setUTCSeconds(ar[2]);
        date.setUTCHours(ar[0]);
        return date;
    }

    [NonSerialized]
    public function getCultureClass():CultureVO {
        return Core.data.catalog.agroCultures[cultureID];
    }

    [NonSerialized]
    public function getTransportCatecoryClass():TSCategoryVO {
        return Core.data.catalog.tsCategories[transportCatID];
    }

    [NonSerialized]
    public function updateObject(perfectObject:PerfectObject):Boolean {
        var old:PerfectObject = objectsDic[perfectObject.uid];
        if (!old) return false;
        old.name = perfectObject.name;
        old.color = perfectObject.color;
        old.uid = perfectObject.uid;
        old.visible = perfectObject.visible;
        if (old.mainType == PerfectObject.POLY) {
            old.startDate = perfectObject.startDate;
            old.endDate = perfectObject.endDate;
            old.cultureID = perfectObject.cultureID;
            old.sizeGa = perfectObject.sizeGa;
        } else {
            old.transportCatID = perfectObject.transportCatID;
            old.transportGosNumber = perfectObject.transportGosNumber;
        }
        return true;
    }

    [NonSerialized]
    public function updateGMSObject(perfectObject:PerfectObject):Boolean {
        var old:PerfectObject = objectsDic[perfectObject.uid];

        if (!old) return false;
        old._center = old._p = perfectObject.center;
        old.angle = perfectObject.angle;
        old.speed = perfectObject.speed;
        old.updateTime = perfectObject.updateData;
        old.updateData = perfectObject.updateData;
        return true;
    }

    [NonSerialized]
    public function update(o:Object):void {
        visible = o.visible;
        if (o.color) color = o.color;
        var date:Date;
        if (o.name)
            name = o.name;
        if (o.cultureID)
            cultureID = o.cultureID;
        if (o.startDate) {
            date = new Date();
            date.setTime(o.startDate);
            startDate = date;
        }
        if (o.endDate) {
            date = new Date();
            date.setTime(o.endDate);
            endDate = date;
        }
        if (o.transportCatID) transportCatID = o.transportCatID;
        if (o.transportGosNumber) transportGosNumber = o.transportGosNumber
    }

    [NonSerialized]
    public function updateCenter(x:Number, y:Number):void {
        if (!_center) _center = {};
        if (!_p) _p = new Point();
        _center.x = _p.x = x;
        _center.y = _p.y = y;
    }

    [NonSerialized]
    private function adapt(s:String):Number {
        var coord:Number = new Number(s);
        coord = coord / 100;
        return Math.floor(coord) + (((coord - Math.floor(coord)) / 60) * 100);
    }
}
}
