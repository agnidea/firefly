/**
 * Created by YP on 11/22/2014.
 */
package ru.agnidea.firefly.core.vo {
import com.gamua.flox.Access;
import com.gamua.flox.Entity;

public class TracksVO extends Entity {
    public function TracksVO() {
        publicAccess = Access.READ_WRITE;
    }

    public var name:String;
    public var color:int;
}
}
