/**
 * Created by flower on 25.11.14.
 */
package ru.agnidea.firefly.core {
import flash.desktop.NativeApplication;
import flash.events.Event;
import flash.net.URLLoader;
import flash.net.URLRequest;

import ru.agnidea.firefly.Core;
import ru.agnidea.firefly.tablet.events.GuiEvents;

//import ru.agnidea.firefly.client.Core;
public class VersionChecker {
    public static var currentVersion:String;
    public static var remoteBuild:String;
    public static var needUpdate:Boolean = false;
    public static var checked:Boolean = false;
    [Bindable]
    public static var lastVersion:Boolean = false;
    private static var _instance:VersionChecker;

    public static function init():void {
        _instance = new VersionChecker();
    }

    public function VersionChecker() {
        try {
            var loader:URLLoader = new URLLoader(new URLRequest("http://agnidea.ru/firefly/build.txt"));
            loader.addEventListener(Event.COMPLETE, loader_completeHandler);
        } catch (e:Error) {

        }

        var appDescriptor:XML = NativeApplication.nativeApplication.applicationDescriptor;
        var ns:Namespace = appDescriptor.namespace();
        currentVersion = appDescriptor.ns::versionNumber;
    }

    private function loader_completeHandler(event:Event):void {
        trace(event.target.data);
        remoteBuild = event.target.data as String;

        var ar:Array = currentVersion.split(".");
        var currentBuild:int = int(ar[2]);
        if (int(remoteBuild) > currentBuild) {
            Core.call(GuiEvents.NEED_UPDATE);
            needUpdate = true;
        } else {
            lastVersion = true;
        }
        checked = true;
    }
}
}
