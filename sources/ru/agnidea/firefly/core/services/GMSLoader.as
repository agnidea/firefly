/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.core.services {
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.HTTPStatusEvent;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLLoader;
import flash.net.URLLoaderDataFormat;
import flash.net.URLRequest;
import flash.net.URLRequestMethod;
import flash.net.URLVariables;

import ru.agnidea.firefly.core.vo.User;

public class GMSLoader extends EventDispatcher {

    public function GMSLoader() {
    }

    public var onComplete:Function
    private var urlLoader:URLLoader = new URLLoader();

    public function getAct(act:String):void {
        var requestVars:URLVariables = new URLVariables();
        requestVars.login = User.login;
        requestVars.password = User.pass;
        requestVars.act = act;

        var request:URLRequest = new URLRequest("http://gms-net.ru/firefly/");
        request.data = requestVars;
        request.method = URLRequestMethod.POST;

        urlLoader = new URLLoader();
        urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
        urlLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler, false, 0, true);
        urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler, false, 0, true);
        urlLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler, false, 0, true);
        urlLoader.addEventListener(Event.COMPLETE, loaderCompleteHandler, false, 0, true);
        try {
            urlLoader.load(request);
        } catch (e:Error) {

        }


    }

    function loaderCompleteHandler(e:Event):void {
        var data:Object = JSON.parse(e.currentTarget.data);
        if (onComplete != null) {
            onComplete(data);
        }
    }

    function httpStatusHandler(e:HTTPStatusEvent):void {
        trace("httpStatusHandler:" + e);
    }

    function securityErrorHandler(e:SecurityErrorEvent):void {
        trace("securityErrorHandler:" + e);
    }

    function ioErrorHandler(e:IOErrorEvent):void {
        trace("!");
        //dispatchEvent(e);
    }

}
}
