/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.core.services {
import com.mapquest.tilemap.RemoteCollection;
import com.mapquest.tilemap.deserializers.DeserializeKML;

import flash.events.Event;
import flash.filesystem.File;

import ru.agnidea.utils.FTimer;

public class KmlLoader {
    public function KmlLoader(kmldir:String, array:Array, poiLoadComplete:Function) {
        collection = array;
        onComplete = poiLoadComplete;
        var dir:File = new File(File.applicationDirectory.nativePath + "/kml/" + kmldir);

        if (dir.exists) {
            for each (var f:File in dir.getDirectoryListing()) {
                var kml:RemoteCollection = new RemoteCollection(f.url, DeserializeKML);
                kml.addEventListener(RemoteCollection.REMOTE_COLLECTION_LOADED, kml_remoteCollectionLoadedHandler);
                kml.addEventListener(RemoteCollection.REMOTE_COLLECTION_FAILED, kml_remoteCollectionFailedHandler);
            }
        } else {
            trace(dir.nativePath, " NOT FOUND !")
        }

    }

    private var collection:Array = [];
    private var onComplete:Function;

    private function kml_remoteCollectionLoadedHandler(event:Event):void {
        var kml:RemoteCollection = event.target as RemoteCollection;
        collection.push(kml);
        FTimer.updateTask(onComplete, 500);
    }

    private function kml_remoteCollectionFailedHandler(event:Event):void {

    }
}
}
