/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.core.services {
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.NetStatusEvent;
import flash.net.GroupSpecifier;
import flash.net.NetConnection;
import flash.net.NetGroup;

public class Stratus extends EventDispatcher {
    private const SERVER:String = "rtmfp://p2p.rtmfp.net/460cd8c014eaa2bca4bd4696-0ab96007aa3c/";
    private const DEVKEY:String = "460cd8c014eaa2bca4bd4696-0ab96007aa3c";
    private const NETGROUP:String = "firefly";
    public var connected:Boolean = false;
    public var userName:String;

    // NetGroup specifier for NETGROUP
    /**
     * NetStatusEvent.NET_STATUS event handler for _nc and _netGroup
     * */

    public var message:Object;

    // Connected to Stratus server and to NETGROUP
    private var _nc:NetConnection;

    // _userName name in chat.
    private var _netGroup:NetGroup;

    // Used to store our P2P Peer ID for binding to UI.
    private var groupSpecWithAuthorizations:String;

    // Counter to make every NetGroup post message unique
    private var _nearID:String;
    private var _msgOutCount:int = 0;

    /**
     * Connect with Stratus server
     * */
    public function connect():void {
        _nc = new NetConnection();
        _nc.addEventListener(NetStatusEvent.NET_STATUS, ncNetStatus);
        _nc.connect(SERVER + DEVKEY);
    }

    /**
     * Post a message to NETGROUP;
     * @param messageText String. Text message to send.
     * */
    public function sendMessageToGroup(messageText:String):void {
        // Construct message object
        var netGroupMessage:Object = new Object();
        netGroupMessage.sender = _netGroup.convertPeerIDToGroupAddress(_nc.nearID);
        netGroupMessage.user = userName;
        netGroupMessage.text = messageText;
        netGroupMessage.sequence = ++_msgOutCount;// Only unique message objects are sent.
        // Send netGroupMessage object to all members of the NETGROUP
        _netGroup.post(netGroupMessage);
    }

    // ========================================
    //  NETGROUP Methods
    // ========================================

    /**
     * Connect with the NETGROUP
     * */
    private function setupGroup():void {
        var groupspec:GroupSpecifier = new GroupSpecifier(NETGROUP);
        // Allow group members to open channels to server
        groupspec.serverChannelEnabled = true;
        // Allow group members to post
        groupspec.postingEnabled = true;

        // Create the group specifi
        groupSpecWithAuthorizations = groupspec.groupspecWithAuthorizations();

        // Join the group specified by groupspec
        _netGroup = new NetGroup(_nc, groupSpecWithAuthorizations);

        // Register listener for NetGroup NetStatus events
        _netGroup.addEventListener(NetStatusEvent.NET_STATUS, ncNetStatus);
    }

    private function ncNetStatus(event:NetStatusEvent):void {
        trace(event.info.code);
        switch (event.info.code) {
            case "NetConnection.Connect.Success":
                _nearID = _nc.nearID;// or you can use event.target.nearID;
                setupGroup();
                dispatchEvent(new Event("connected"));
                break;
            case "NetGroup.Connect.Success":
                connected = true;
                break;
            case "NetGroup.Posting.Notify":
                message = event.info.message;
                dispatchEvent(new Event("message"));
                break;
            // FYI: More NetGroup event info codes
            case "NetGroup.Neighbor.Connect":
            case "NetGroup.LocalCoverage.Notify":
            case "NetGroup.SendTo.Notify": // event.info.message, event.info.from, event.info.fromLocal
            case "NetGroup.MulticastStream.PublishNotify": // event.info.name
            case "NetGroup.MulticastStream.UnpublishNotify": // event.info.name
            case "NetGroup.Replication.Fetch.SendNotify": // event.info.index
            case "NetGroup.Replication.Fetch.Failed": // event.info.index
            case "NetGroup.Replication.Fetch.Result": // event.info.index, event.info.object
            case "NetGroup.Replication.Request": // event.info.index, event.info.requestID
            default:
            {
                break;
            }
        }
    }

}
}


