package ru.agnidea.firefly.core.services {

import com.mapquest.tilemap.RemoteCollection;
import com.mapquest.tilemap.deserializers.DeserializeKML;

import feathers.controls.Alert;

import flash.events.Event;
import flash.filesystem.File;

import ru.agnidea.firefly.core.vo.PerfectObject;

/**
 * @author flower
 */
public class KmlService {
    public function KmlService() {
        currentPlace = new PerfectObject(PerfectObject.COLLECTION);
    }

    public var onPlacesLoaded:Function;
    public var poiKml:Array = [];
    public var perfectPlaces:PerfectObject;
    public var currentPlace:PerfectObject;
    private var file:File = new File();

    public function getAll():void {
        new KmlLoader("static", poiKml, poiLoadComplete);
    }

    public function getByHardname(hardname:String):PerfectObject {
        for each (var perfectObject:PerfectObject in currentPlace.objects) {
            if (perfectObject.uid == hardname)
                return perfectObject;
        }
        return null;
    }

    public function loadFormFile():void {
        file.addEventListener(Event.SELECT, openFile);
        file.browseForOpen("Open");
    }

    private function poiLoadComplete():void {
        perfectPlaces = new PerfectObject(PerfectObject.COLLECTION);
        perfectPlaces.name = "Все места";
        for each (var remote:RemoteCollection in poiKml) {
            var current:PerfectObject = new PerfectObject(PerfectObject.COLLECTION);
            var ar:Array = remote.url.split("/");
            var name:String = ar[ar.length - 1];
            switch (name) {
                case "kml.kml":
                    current.name = "Достопримечательности";
                    break;
                case "morskoy.kml" :
                    current.name = "Колхоз Морской";
                    currentPlace = current;
                    break;
                case "ferm1.kml":
                    current.name = "Ферма";
                    break;
            }
            current.name = name;
            current.updateCenter(remote.boundingRect.ul.lat, remote.boundingRect.ul.lng);
            for each (var o:* in remote.getAll()) {
                var geo:PerfectObject = new PerfectObject(PerfectObject.PLACE);
                geo.initGeo(o);
                current.add(geo);
            }
            perfectPlaces.addChildren(current);
        }
        perfectPlaces.addChildren(perfectPlaces);
        if (onPlacesLoaded != null) onPlacesLoaded(perfectPlaces);
    }

    // Display information about event and file name in text window

    private function openFile(event:Event):void {
        trace(file.downloaded)

        try {
            var remote:RemoteCollection = new RemoteCollection(file.url, DeserializeKML);
            remote.addEventListener(RemoteCollection.REMOTE_COLLECTION_LOADED, kml_remoteCollectionLoadedHandler);
        } catch (e:Error) {
            Alert.show("Ошибка загрузки файла", e.message);
        }
    }


    private function kml_remoteCollectionLoadedHandler(event:Event):void {
        var kml:RemoteCollection = event.target as RemoteCollection;
        var k:Object = kml.getAll();
        for each (var o:* in kml.getAll()) {
            var geo:PerfectObject = new PerfectObject(PerfectObject.PLACE);
            geo.initGeo(o);
            if (onPlacesLoaded != null) onPlacesLoaded(geo);
        }
    }
}
}