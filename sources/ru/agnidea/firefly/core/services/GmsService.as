/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.core.services {
import flash.events.TimerEvent;
import flash.utils.Timer;

import ru.agnidea.firefly.Core;
import ru.agnidea.firefly.core.vo.PerfectObject;

public class GmsService {
    public function GmsService() {

        playerLoader = new GMSLoader();
        playerLoader.onComplete = onData;
    }

    public var onObjectsLoaded:Function;
    public var onError:Function;
    public var onLogin:Function;

    public var currentPerfect:PerfectObject;
    public var timer:Timer;
    private var playerLoader:GMSLoader;

    public function getCord():void {
        playerLoader.getAct("coords");
    }

//    public function getNames():void {
//        playerLoader.getAct("equips");
//    }

    public function start():void {
        timer = new Timer(10000);
        timer.addEventListener(TimerEvent.TIMER, timer_timerHandler);
        timer.start();
    }

    private function onData(data:Object):void {
        if (data.err) {
            if (onError != null) onError(data.err);
            return;
        }
        var perfect:PerfectObject;
        for each (var object:Object in data) {
            if (object) {
                perfect = Core.data.catalog.movingObjects[object.equip_id];
                if (perfect) {
                    perfect.initGMS(object);
                } else {
                    perfect = new PerfectObject(PerfectObject.PLAYER);
                    perfect.initGMS(object);
                    Core.data.catalog.movingObjects.add(perfect);
                }
            }
        }
        if (onObjectsLoaded != null) onObjectsLoaded();
        if (onLogin != null) onLogin();
    }

    private function timer_timerHandler(event:TimerEvent):void {
        getCord();
    }
}
}
