package ru.agnidea.firefly.core {

import flash.events.Event;
import flash.geom.Point;

import org.mvcexpress.mvc.Proxy;

import ru.agnidea.firefly.core.services.Stratus;
import ru.agnidea.firefly.core.vo.User;
import ru.agnidea.firefly.tablet.chat.model.ChatEvents;
import ru.agnidea.firefly.tablet.chat.model.MessageVO;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class P2PProxy extends Proxy {
    public function P2PProxy() {
    }

    public var sendOnConnect:String;
    public var service:Stratus;

    override protected function onRegister():void {
        service = new Stratus();
        service.userName = User.login;
        service.connect();
        service.addEventListener("connected", onConnected);
        service.addEventListener("message", onOnMessage);

    }

    override protected function onRemove():void {

    }

    public function sendChat(text:String):void {
        if (!service.connected) return;
        var o:Object = new Object();
        o.type = "string";
        o.message = text;
        service.sendMessageToGroup(JSON.stringify(o));
    }

    public function sendGPS(latlon:Point):void {
        if (!service.connected) return;
        var o:Object = new Object();
        o.type = "gps";
        o.x = latlon.x;
        o.y = latlon.y;
        service.sendMessageToGroup(JSON.stringify(o));
    }

    public function sendString(s:String):void {
        if (!service.connected) return;
        var o:Object = new Object();
        o.type = "string";
        o.data = s;
        service.sendMessageToGroup(JSON.stringify(o));
    }

    private function onOnMessage(event:Event):void {
        var data:Object = JSON.parse(service.message.text);

        var message:MessageVO = new MessageVO();
        message.fromString(data.data);
        if (message.data && message.data.perfectO)
            sendMessage(ChatEvents.UPDATE_PERFECT, message);
        sendMessage(ChatEvents.NEW_MESSAGE, message);
    }

    private function onConnected(event:Event):void {
        sendChat("im connected");
        if (sendOnConnect) {
            sendString(sendOnConnect);
            sendOnConnect = "";
        }
    }
}
}