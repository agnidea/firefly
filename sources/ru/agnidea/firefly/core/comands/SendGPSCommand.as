package ru.agnidea.firefly.core.comands {

import flash.geom.Point;

import org.mvcexpress.mvc.Command;

import ru.agnidea.firefly.core.P2PProxy;
import ru.agnidea.firefly.core.vo.User;
import ru.agnidea.firefly.tablet.chat.model.MessageVO;

/**
 * @author flower
 */
public class SendGPSCommand extends Command {

//    [Inject] public var remoteDB:RemoteDBProxy;
//    [Inject] public var messages:MessagesProxy;
//    https://link.getsync.com/#f=YCanvas&sz=28E6&s=XWNKC3UGUW4IBNOZLXRTFPIQO7BFPNHH&i=CD7D34ZRXWFQFYTPGLA7DLKXUOHMJZJ4Y&p=CC6JVFQ36SQXUFBKUYNQ4BSPTMEHI7KM
    [Inject] public var p2p:P2PProxy;
    public function execute(latLon:Point):void {
        var message:MessageVO = new MessageVO();
        message.mainType = MessageVO.EVENT;
        message.data = latLon;
        message.senderName = User.login;
        message.senderInfo = User.info;
        message.senderDID = User.deviceID;
        message.text = "Пользователь " + User.login + " вход c устройства: " + User.info + " доступно месторасположение";
        message.saveQueued();

        p2p.sendString(message.toString());

    }

}
}
