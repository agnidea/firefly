/**
 * Created by flower on 09.10.14.
 */
package ru.agnidea.firefly.core.comands {
import flash.system.Capabilities;

import org.mvcexpress.mvc.Command;

import ru.agnidea.firefly.core.SettingsProxy;
import ru.agnidea.firefly.core.VersionChecker;
import ru.agnidea.firefly.core.data.MainDataProxy;
import ru.agnidea.firefly.core.vo.User;
import ru.agnidea.firefly.tablet.chat.model.MessagesProxy;

public class AppStartCommand extends Command {

    [Inject]
    public var settings:SettingsProxy;
    [Inject]
    public var messagesProxy:MessagesProxy;
    [Inject]
    public var mainData:MainDataProxy;

    public function execute(o:* = null):void {
        VersionChecker.init();
        User.login = settings.data.login;
        if (!settings.data.deviceID) {
            settings.saveDeviceID(Capabilities.os + " : " + Math.random() * 1);
        }
        User.deviceID = settings.data.deviceID;
        mainData.loadCatalogs()
    }
}
}

