package ru.agnidea.firefly.core.comands {
import org.mvcexpress.mvc.Command;

import ru.agnidea.firefly.core.data.MainDataProxy;
import ru.agnidea.firefly.tablet.events.GuiEvents;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class GmsErrorCommand extends Command {


    [Inject]
    public var data:MainDataProxy;

    public function execute(info:String = null):void {
        sendMessage(GuiEvents.SHOW_ERROR, info)
    }

}
}
