package ru.agnidea.firefly.core.comands {
import com.gamua.flox.Player;

import org.mvcexpress.mvc.Command;

import ru.agnidea.firefly.Core;
import ru.agnidea.firefly.core.data.MainDataProxy;
import ru.agnidea.firefly.core.vo.User;
import ru.agnidea.firefly.tablet.events.GuiEvents;
import ru.agnidea.firefly.tablet.events.PerfectEvents;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class LoginCommand extends Command {


    [Inject]
    public var data:MainDataProxy;

    public function execute(login:String = null):void {
        data.gms.onLogin = onLogin;
        data.gms.getCord();
    }

    public function onLogin():void {
        if (Player.current.id == User.login) {
            data.updateData();
        } else {
            Player.logout();
            Player.current.id = User.login;
            Player.current.ownerId = User.login;
            Player.loginWithKey(User.login,
                    function onComplete(player:Player) {
                        data.updateData();
                        Core.settings.saveLogin(User.login);
                        Core.settings.savePass(User.pass);
                    },
                    function onError(message:String) {
                        trace("FLOX Login Error:", message)
                    }
            );
        }

        data.gms.onLogin = null;
        sendMessage(PerfectEvents.REFRESH_PLAYERS);
        data.gms.start();
        sendMessage(GuiEvents.SHOW_MAIN);
    }

}
}
