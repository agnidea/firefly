package ru.agnidea.firefly.core.comands {
import org.mvcexpress.mvc.Command;

import ru.agnidea.firefly.core.data.MainDataProxy;
import ru.agnidea.firefly.tablet.events.PerfectEvents;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class RefreshPlayersCommand extends Command {
    [Inject]
    public var data:MainDataProxy;

    public function execute(o:* = null):void {
        if (data.catalog.movingObjects.length > 0) {
            sendMessage(PerfectEvents.PLAYERS_LOADED, data.catalog.movingObjects);
        }
    }

}
}
