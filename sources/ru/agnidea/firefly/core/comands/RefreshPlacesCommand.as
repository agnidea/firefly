package ru.agnidea.firefly.core.comands {

import org.mvcexpress.mvc.Command;

import ru.agnidea.firefly.core.data.MainDataProxy;
import ru.agnidea.firefly.tablet.events.PerfectEvents;

public class RefreshPlacesCommand extends Command {

    [Inject]
    public var data:MainDataProxy;

    public function execute(blank:Object = null):void {
        if (data.catalog.geoObjects.length > 0) {
            sendMessage(PerfectEvents.PLACES_LOADED, data.catalog.geoObjects);
        }
    }

}
}
