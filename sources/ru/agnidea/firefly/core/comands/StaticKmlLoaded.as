package ru.agnidea.firefly.core.comands {
import org.mvcexpress.mvc.Command;

import ru.agnidea.firefly.core.services.KmlService;
import ru.agnidea.firefly.tablet.events.PerfectEvents;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class StaticKmlLoaded extends Command {

    [Inject]
    public var data:KmlService;

    public function execute(o:* = null):void {
        trace("KML OK");
        if (data.currentPlace)
            sendMessage(PerfectEvents.PLACES_LOADED, data.currentPlace);
    }

}
}
