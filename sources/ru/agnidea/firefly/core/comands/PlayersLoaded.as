/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.core.comands {
import org.mvcexpress.mvc.Command;

import ru.agnidea.firefly.core.services.GmsService;
import ru.agnidea.firefly.tablet.events.PerfectEvents;

public class PlayersLoaded extends Command {
    public function PlayersLoaded() {
    }

    [Inject]
    public var data:GmsService;

    public function execute(o:* = null):void {
        sendMessage(PerfectEvents.CURRENT_PLAYER_CHANGE, data.currentPerfect);
    }

}
}
