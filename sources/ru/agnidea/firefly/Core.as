/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly {

import com.gamua.flox.Flox;

import flash.display.Stage;

import org.mvcexpress.modules.ModuleCore;

import ru.agnidea.firefly.core.GPSProxy;
import ru.agnidea.firefly.core.P2PProxy;
import ru.agnidea.firefly.core.SettingsProxy;
import ru.agnidea.firefly.core.comands.AppStartCommand;
import ru.agnidea.firefly.core.comands.GmsErrorCommand;
import ru.agnidea.firefly.core.comands.LoginCommand;
import ru.agnidea.firefly.core.comands.RefreshPlacesCommand;
import ru.agnidea.firefly.core.comands.RefreshPlayersCommand;
import ru.agnidea.firefly.core.comands.SendGPSCommand;
import ru.agnidea.firefly.core.data.MainDataProxy;
import ru.agnidea.firefly.mediators.StageMediator;
import ru.agnidea.firefly.tablet.chat.commands.ChatInit;
import ru.agnidea.firefly.tablet.chat.model.MessagesProxy;
import ru.agnidea.firefly.tablet.events.NetworkEvents;
import ru.agnidea.firefly.tablet.events.PerfectEvents;

public class Core extends ModuleCore {
    public static var base:Core;
    public static var view:StageMediator;
    public static var data:MainDataProxy;
    public static var settings:SettingsProxy;
    public static var messagesProxy:MessagesProxy;
    public static var gps:GPSProxy;
    public static var p2p:P2PProxy;

    public static function call(commandName:String, data:Object = null):void {
        base.sendMessage(commandName, data);
    }

    public static function add(commandName:String, funcrion:Function):void {
        StageMediator.addH(commandName, funcrion);
    }

    public static function start(stage:Stage):void {
        base.mediatorMap.mediateWith(stage, StageMediator);
    }

    public function Core() {
        base = this;
    }

    override protected function onInit():void {
        super.onInit();

        Flox.init("wTNwzPVINN7pfO00", "19BwhSxtdpBbpTnS", "0.11");

        data = new MainDataProxy();
        proxyMap.map(data);

        settings = new SettingsProxy();
        proxyMap.map(settings);

        messagesProxy = new MessagesProxy();
        proxyMap.map(messagesProxy);
        commandMap.execute(ChatInit);

        p2p = new P2PProxy();
        proxyMap.map(p2p);

        gps = new GPSProxy();
        proxyMap.map(gps);
        commandMap.map(NetworkEvents.GPS_UPDTATE, SendGPSCommand);


        commandMap.map(PerfectEvents.REFRESH_PLAYERS, RefreshPlayersCommand);
        commandMap.map(PerfectEvents.REFRESH_PLACES, RefreshPlacesCommand);
        commandMap.map(NetworkEvents.LOGIN, LoginCommand);
        commandMap.map(NetworkEvents.GMS_ERROR, GmsErrorCommand);

        commandMap.execute(AppStartCommand, true);
    }
}
}
