/**
 * Created by flower on 10.11.14.
 */
package ru.agnidea.firefly.mediators {
import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.core.AppState;
import ru.agnidea.firefly.tablet.chat.ChatViewMediator;
import ru.agnidea.firefly.tablet.chat.view.ChatView;
import ru.agnidea.firefly.tablet.events.GisEvents;
import ru.agnidea.firefly.tablet.events.GuiEvents;
import ru.agnidea.firefly.tablet.gis.staling.YStarling;
import ru.agnidea.firefly.tablet.view.main.UpdateView;
import ru.agnidea.firefly.view.LoginMediator;
import ru.agnidea.firefly.view.LoginView;

import spark.components.Alert;
import spark.components.Application;
import spark.components.Group;

import starling.core.Starling;

public class CoreApplicationMediator extends Mediator {


    public var app:Application;


    public var chatView:ChatView;
//    private var editView:EditView;
    public var login:LoginView;
    public var starling:YStarling;

    public var mainGroup:Group;

    override public function onRegister():void {
        addHandler(GuiEvents.SHOW_CHAT, onChatShow);
//        addHandler(GuiEvents.SHOW_EDIT, onEditShow);
        addHandler(GuiEvents.SHOW_UPDATE, onUpdateShow);
        addHandler(GuiEvents.SHOW_ERROR, onErrorShow);
        login = new LoginView();
        app.addElement(login);
        mediatorMap.mediateWith(login, LoginMediator);

        Starling.handleLostContext = true;
        Starling.multitouchEnabled = true;
    }

    private function onErrorShow(text:String):void {
        Alert.show(text, "Ошибка!!!", Alert.OK);//, this, onAlertClosed , imgCls, 2 );
    }

    private function onUpdateShow(o:Object = null):void {
        app.removeAllElements();
        app.addElement(new UpdateView());
    }

//    private function onEditShow(o:Object = null):void {
//        app.removeAllElements();
//        AppState.infoCardState = "edit";
//        sendMessage(GisEvents.HIDE, true);
//        if (!editView) {
//            editView = new EditView();
//            mediatorMap.mediateWith(editView, EditViewMediator);
//        }
//        app.addElement(editView);
//
//    }




    private function onChatShow(show:Boolean = true):void {
        AppState.infoCardState = "chat";
        if (!chatView) {
            chatView = new ChatView();
            mediatorMap.mediateWith(chatView, ChatViewMediator)
        }
        sendMessage(GisEvents.HIDE, true);
        app.removeElement(mainGroup);
        app.addElement(chatView);
    }
}
}
