/**
 * Created by flower on 12.12.14.
 */
package ru.agnidea.firefly.mediators {
import ru.agnidea.firefly.core.AppState;
import ru.agnidea.firefly.tablet.events.GisEvents;
import ru.agnidea.firefly.tablet.events.GuiEvents;
import ru.agnidea.firefly.tablet.gis.staling.YStarling;
import ru.agnidea.firefly.tablet.gis.staling.YStarlingMediator;
import ru.agnidea.firefly.tablet.view.QuickEdit;
import ru.agnidea.firefly.tablet.view.main.InfoCardMediator;
import ru.agnidea.firefly.tablet.view.main.MobileView;
import ru.agnidea.firefly.tablet.view.main.MobileViewMediator;

public class MobileApplicationMediator extends CoreApplicationMediator {


    [Inject]
    public var view:MobileApp;


    public var mainView:MobileView;

    override public function onRegister():void {
        app = view;
        super.onRegister();
        addHandler(GuiEvents.SHOW_MAIN, onMainShow);
    }

    private function onMainShow(o:Object = null):void {
        app.removeAllElements();
        AppState.infoCardState = "map";
        if (login) login = null;
        if (!mainView) {
            mainGroup = mainView = new MobileView();
            mediatorMap.mediateWith(mainView, MobileViewMediator);
        }

        InfoCardMediator.stage = view.stage;
        QuickEdit.init(view);
        app.addElement(mainView);

        if (!starling) {
            starling = new YStarling(app.stage);
            mediatorMap.mediateWith(starling, YStarlingMediator);
        }
        sendMessage(GisEvents.HIDE, false);
    }


}
}
