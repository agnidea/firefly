/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.tablet.gis {

import flash.system.Capabilities;

import ru.agnidea.firefly.Core;
import ru.agnidea.firefly.tablet.gis.layers.ObjectLayer;

import sk.yoz.utils.GeoUtils;
import sk.yoz.ycanvas.map.YCanvasMap;
import sk.yoz.ycanvas.map.demo.partition.CustomPartitionFactory;
import sk.yoz.ycanvas.map.demo.partition.PartitionLoader;
import sk.yoz.ycanvas.map.display.MapLayer;
import sk.yoz.ycanvas.map.display.StrokeLayer;
import sk.yoz.ycanvas.map.events.CanvasEvent;
import sk.yoz.ycanvas.map.layers.LayerFactory;
import sk.yoz.ycanvas.map.managers.AbstractTransformationManager;
import sk.yoz.ycanvas.map.managers.MouseTransformationManager;
import sk.yoz.ycanvas.map.managers.TouchTransformationManager;
import sk.yoz.ycanvas.map.valueObjects.Limit;
import sk.yoz.ycanvas.map.valueObjects.MapConfig;
import sk.yoz.ycanvas.map.valueObjects.Transformation;

public class MapHelper {

    public function MapHelper() {
        var partitionLoader:PartitionLoader = new PartitionLoader();
        if (Core.settings.data.startYX) {
            transformation.centerX = Core.settings.data.startX;
            transformation.centerY = Core.settings.data.startY;
        }
        if (Core.settings.data.startZoom) {
            transformation.scale = Core.settings.data.startZoom;
        } else {
            transformation.scale = 1 / 4096;
        }
        transformation.rotation = 0;

        var limit:Limit = new Limit;
        limit.minScale = 1;
        limit.maxScale = 1 / 65536;
        limit.minCenterX = 0;
        limit.maxCenterX = GeoUtils.lon2x(180);
        limit.minCenterY = GeoUtils.lat2y(85);
        limit.maxCenterY = GeoUtils.lat2y(-85);

        var config:MapConfig = Core.settings.data.getCurrentMapType();

        map = new YCanvasMap(config, transformation, 256);


        map.partitionFactory = new CustomPartitionFactory(config, map, partitionLoader);
        map.layerFactory = new LayerFactory(config, map.partitionFactory);

        map.addEventListener(CanvasEvent.TRANSFORMATION_FINISHED, onMapTransformationFinished);

        if (Capabilities.os.indexOf("Mac") >= 0) {
            transformationManager = new MouseTransformationManager(map, limit)
        } else {
            transformationManager = new TouchTransformationManager(map, limit);
        }


        polygonLayer = new MapLayer;
        map.addMapLayer(polygonLayer);

        strokeLayer = new StrokeLayer;
        strokeLayer.autoUpdateThickness = false;
        map.addMapLayer(strokeLayer);

        objectsLayer = new ObjectLayer;
        map.addMapLayer(objectsLayer);
    }

    public var transformation:Transformation = new Transformation;
    public var map:YCanvasMap;
    public var polygonLayer:MapLayer;
    public var strokeLayer:StrokeLayer;
    public var objectsLayer:ObjectLayer;
    public var transformationManager:AbstractTransformationManager;

    public function dispose():void {
        map.removeEventListener(CanvasEvent.TRANSFORMATION_FINISHED, onMapTransformationFinished);
        map.dispose();
        map = null;

        transformationManager.dispose();
        transformationManager = null;

        polygonLayer = null;
        strokeLayer = null;
        objectsLayer = null;
    }

    private function onMapTransformationFinished(event:CanvasEvent):void {
        if (!strokeLayer.autoUpdateThickness)
            strokeLayer.updateThickness();
    }

}
}
