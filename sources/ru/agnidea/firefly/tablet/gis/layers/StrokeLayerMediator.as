package ru.agnidea.firefly.tablet.gis.layers {
import flash.utils.Dictionary;

import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.firefly.tablet.events.PerfectEvents;
import ru.agnidea.firefly.tablet.gis.objects.TrackGis;
import ru.agnidea.utils.DataCollection;

import sk.yoz.ycanvas.map.display.StrokeLayer;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class StrokeLayerMediator extends Mediator {

    [Inject]
    public var view:StrokeLayer;
    private var pool:Dictionary = new Dictionary();

    override public function onRegister():void {
        addHandler(PerfectEvents.PLACES_LOADED, onPlacesLoaded);
        addHandler(PerfectEvents.PERFECT_HIDE, onPerfectHide);
    }

    override public function onRemove():void {

    }

    private function onPerfectHide(perfect:PerfectObject):void {
        var track:TrackGis = TrackGis.perfects[perfect];
        if (!track) return;
        track.stroke.visible = perfect.visible;
    }

    private function onPlacesLoaded(data:DataCollection):void {
        for each (var perfect:PerfectObject in data) {
            if (!pool[perfect.name] && perfect.mainType == PerfectObject.LINE)
                pool[perfect.name] = new TrackGis(perfect, view);
        }
    }
}
}