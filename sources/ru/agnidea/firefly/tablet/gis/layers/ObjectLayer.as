package ru.agnidea.firefly.tablet.gis.layers {
import sk.yoz.ycanvas.map.display.*;

import starling.display.DisplayObject;

/**
 * A map layer extension to be used as a marker container. Marker real/global
 * size remains the same no matter the YCanvas transformation is. Marker
 * global rotation is 0.
 */
public class ObjectLayer extends MapLayer {
    private var list:Vector.<DisplayObject> = new Vector.<DisplayObject>;

    /**
     * @inheritDoc
     */
    override public function set rotation(value:Number):void {
        if (rotation == value)
            return;

        super.rotation = value;
//        for (var i:uint = list.length; i--;)
//            list[i].rotation = -rotation;
    }

    /**
     * @inheritDoc
     */
    override public function set scale(value:Number):void {
//        if (isFlattened) unflatten();

        if (scale == value)
            return;

        super.scale = value;
        var s:Number = 1 / scale;
        var p:uint = (s / 65536) * 100;
        var out:Number;
        if (p < 6) {
            out = s - (p * (p / 0.03));
        } else {
            out = s - (p * (p / 0.24));
        }
        for (var i:uint = list.length; i--;) {
            list[i].scaleX = list[i].scaleY = out;
        }

//        doLater(flatten, 700);
    }

    /**
     * Adds any Starling DisplayObject as a marker.
     */
    public function add(item:DisplayObject):void {
//        if (isFlattened) unflatten();
        item.scaleX = item.scaleY = 1 / scale;
//        item.rotation = -rotation;
        list.push(item);
        addChild(item);
//        doLater(flatten, 300);
    }

    /**
     * Removes previously added marker.
     */
    public function remove(item:DisplayObject):void {
        list.splice(list.indexOf(item), 1);
        removeChild(item);
    }
}
}