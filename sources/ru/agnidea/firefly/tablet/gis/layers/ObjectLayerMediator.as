package ru.agnidea.firefly.tablet.gis.layers {
import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.firefly.tablet.events.PerfectEvents;
import ru.agnidea.firefly.tablet.gis.MapCore;
import ru.agnidea.firefly.tablet.gis.objects.FocusPlayer;
import ru.agnidea.firefly.tablet.gis.objects.Player;
import ru.agnidea.utils.DataCollection;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class ObjectLayerMediator extends Mediator {

    [Inject]
    public var view:ObjectLayer;
    private var focus:FocusPlayer;

    override public function onRegister():void {
        addHandler(PerfectEvents.PLAYERS_LOADED, playersLoaded);
        sendMessage(PerfectEvents.REFRESH_PLAYERS);

        addHandler(PerfectEvents.PLAYER_SELECT, onPlayerSelect);
        addHandler(PerfectEvents.PERFECT_SELECT, onPerfectSelect);

        addHandler(PerfectEvents.PERFECT_CHANGE, onPerfectHide);
        addHandler(PerfectEvents.PERFECT_HIDE, onPerfectHide);

        focus = new FocusPlayer(view);
    }

    private function onPerfectHide(perfect:PerfectObject):void {
        var angels:Player = Player.perfects[perfect];
        if (!angels) return;
        if (angels.perfect.uid == perfect.uid) {
            angels.updateVisible();
            if (perfect.visible) {
                focus.to(angels);
            }
        }

    }


    private function onPerfectSelect(perfect:PerfectObject):void {
        var player:Player = Player.perfects[perfect];
        if (player) {
                MapCore.tm.moveRotateScaleToTween(player.body.x, player.body.y, 0, MapCore.scale + 0.00000001);
            if (perfect.visible)
                focus.to(player);
                return;
            }
        if (perfect.mainType == PerfectObject.SOUL) {
            player = new Player(perfect, view);
            MapCore.tm.moveRotateScaleToTween(player.body.x, player.body.y, 0, MapCore.scale + 0.00000001);
            focus.to(player);
        }

    }

    private function onPlayerSelect(player:Player):void {
        focus.to(player);
    }

    private function playersLoaded(ar:DataCollection):void {
        for each (var perfect:PerfectObject in ar) {
            var p:Player = Player.perfects[perfect];
            if (p) {
                p.updateVisible();
            } else {
                new Player(perfect, view);
            }
        }
    }
}
}