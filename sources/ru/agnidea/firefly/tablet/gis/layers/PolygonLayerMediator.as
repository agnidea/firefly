/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.tablet.gis.layers {
import flash.utils.Dictionary;

import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.firefly.tablet.events.PerfectEvents;
import ru.agnidea.firefly.tablet.gis.MapCore;
import ru.agnidea.firefly.tablet.gis.objects.PolyPlace;
import ru.agnidea.utils.DataCollection;

import sk.yoz.ycanvas.map.display.MapLayer;

public class PolygonLayerMediator extends Mediator {

    [Inject]
    public var view:MapLayer;
    private var lastSelectedPoly:PolyPlace;
    private var pool:Dictionary = new Dictionary();

    override public function onRegister():void {
        addHandler(PerfectEvents.PERFECT_SELECT, perfectSelect);
        addHandler(PerfectEvents.POLY_SELECT, selectPoly);
        addHandler(PerfectEvents.PLACES_LOADED, perfectPlaceChange);
        sendMessage(PerfectEvents.REFRESH_PLACES);
        addHandler(PerfectEvents.PERFECT_HIDE, onPerfectHide)
    }

    private function onPerfectHide(perfect:PerfectObject):void {
        var polyPlace:PolyPlace = PolyPlace.perfects[perfect];
        if (!polyPlace) return;
        polyPlace.poly.visible = perfect.visible;
    }



    private function perfectSelect(perfect:PerfectObject):void {
        if (perfect.mainType == PerfectObject.POLY) {
            var pp:PolyPlace = pool[perfect.label];
            if (!pp) return;
            pp.select(true);
            if (lastSelectedPoly) lastSelectedPoly.select(false);
            lastSelectedPoly = pp;
            if (perfect)
                MapCore.toLatLon(perfect.center.x, perfect.center.y);
        }
    }

    private function selectPoly(poly:PolyPlace):void {
        if (lastSelectedPoly) lastSelectedPoly.select(false);
        lastSelectedPoly = poly;
        poly.select(true);
    }


    private function perfectPlaceChange(perfect:DataCollection):void {
        for each (var perfectChild:PerfectObject in perfect) {
            if (!pool[perfectChild.name] && perfectChild.mainType == PerfectObject.POLY)
                    pool[perfectChild.name] = new PolyPlace(perfectChild, view);
        }
    }
}
}
