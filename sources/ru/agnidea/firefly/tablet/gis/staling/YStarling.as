/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.tablet.gis.staling {
import flash.display.Stage;
import flash.events.Event;

import ru.agnidea.firefly.tablet.gis.*;

import starling.core.Starling;

public class YStarling extends Starling {

    public function YStarling(stage:Stage) {
        super(YCMRoot, stage);


        nativeStage.addEventListener(Event.RESIZE, stage_resizeHandler, false, int.MAX_VALUE, true);
        nativeStage.addEventListener(Event.DEACTIVATE, stage_deactivateHandler, false, 0, true);
    }


    private function stage_resizeHandler(event:Event):void {
        stage.stageWidth = nativeStage.stageWidth;
        stage.stageHeight = nativeStage.stageHeight;


        viewPort.width = nativeStage.stageWidth;
        viewPort.height = nativeStage.stageHeight

//        if (Capabilities.os.indexOf("iPad") >= 0) {
//            var sx:int = 40;
//            if (Capabilities.screenDPI > 140) {
//                sx = 100;
//            }
//            viewPort.height = nativeStage.stageHeight - sx;
//            viewPort.y = sx;
//        }
    }

    private function stage_deactivateHandler(event:Event):void {
        stop();
        nativeStage.addEventListener(Event.ACTIVATE, stage_activateHandler, false, 0, true);
    }

    private function stage_activateHandler(event:Event):void {
        nativeStage.removeEventListener(Event.ACTIVATE, stage_activateHandler);
        start();
    }


}
}
