package ru.agnidea.firefly.tablet.gis.staling {
import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.tablet.gis.*;

import starling.events.Event;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class YStarlingMediator extends Mediator {

    [Inject]
    public var star:YStarling;

    override public function onRegister():void {
        star.enableErrorChecking = false;
        star.start();
        star.addEventListener(Event.ROOT_CREATED, handleRootCreated);
    }


    private function handleRootCreated(event:Event):void {
        star.removeEventListener(Event.ROOT_CREATED, handleRootCreated);
        mediatorMap.mediateWith(star.root, YCMRootMediator);
    }

}
}