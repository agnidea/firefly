package ru.agnidea.firefly.tablet.gis {
import flash.system.Capabilities;

import sk.yoz.ycanvas.map.demo.partition.PartitionLoader;

import starling.core.Starling;
import starling.display.DisplayObject;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.Event;

/**
 * MainFlex Starling class. Provides demo Feathers UI for map controll.
 */
public class YCMRoot extends Sprite {
    public function YCMRoot() {
        if (!stage)
            addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
        else
            onAddedToStage();
        Starling.current.nativeStage.addEventListener("resize", resize);
    }

    public var core:MapHelper;
    private var mapContainer:Sprite;
    private var partitionLoader:PartitionLoader;

    override public function addChild(child:DisplayObject):DisplayObject {
        return super.addChild(child);

    }

    public function removeGis():void {
        core.transformationManager.allowMove = false;
        core.transformationManager.allowRotate = false;
        core.transformationManager.allowZoom = false;
        mapContainer.touchable = false;
        removeChild(mapContainer);
    }

    public function addGis():void {
        core.transformationManager.allowMove = true;
        core.transformationManager.allowRotate = false;
        core.transformationManager.allowZoom = true;
        mapContainer.touchable = true;
        addChild(mapContainer);
    }

    private function resize(...rest):void {
        if (core) {
            core.map.display.x = 0;
            core.map.display.y = 0;
            core.map.display.width = Starling.current.viewPort.width;
            core.map.display.height = Starling.current.viewPort.height;
        }
    }

    private function createMaps():void {
        core = new MapHelper();
        core.transformationManager.allowRotate = false;
        mapContainer.addChild(core.map.display);
        core.map.display.invalidateStarlingViewPort();
    }

    /**
     * Creates UI (children).
     */
    private function onAddedToStage(event:Event = null):void {
        removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
        mapContainer = new Sprite;
        addChild(mapContainer);
        createMaps();
        if (Capabilities.os.indexOf("iPad") >= 0) {
            var statusBar:Quad;
            if (Capabilities.screenDPI > 140) {
                statusBar = new Quad(Starling.current.viewPort.width, 45, 0);
            } else {
                statusBar = new Quad(Starling.current.viewPort.width, 20, 0);
            }
            mapContainer.addChild(statusBar);
        }
        resize();
    }

}
}
