/**
 * Created by flower on 15.10.14.
 */
package ru.agnidea.firefly.tablet.gis {
import flash.geom.Point;

import sk.yoz.utils.GeoUtils;
import sk.yoz.ycanvas.map.YCanvasMap;
import sk.yoz.ycanvas.map.managers.AbstractTransformationManager;

public class MapCore {
    public static var tm:AbstractTransformationManager;
    private static var map:YCanvasMap;

    public static function get center():Point {
        return map.center;
    }

    public static function get scale():Number {
        return map.scale;
    }


    public static function init(core:MapHelper):void {
        tm = core.transformationManager;
        map = core.map;
    }

    public static function toLatLon(lat:Number, lng:Number):void {
        tm.moveRotateScaleToTween(GeoUtils.lon2x(lng), GeoUtils.lat2y(lat), 0, map.scale + 0.00000001);
    }

    public function MapCore() {
    }
}
}
