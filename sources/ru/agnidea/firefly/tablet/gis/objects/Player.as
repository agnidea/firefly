/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.tablet.gis.objects {
import com.greensock.TweenLite;

import flash.utils.Dictionary;

import ru.agnidea.firefly.Core;
import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.firefly.tablet.events.PerfectEvents;
import ru.agnidea.firefly.tablet.gis.layers.ObjectLayer;

import sk.yoz.utils.GeoUtils;

import starling.display.Image;
import starling.display.Sprite;
import starling.events.TouchEvent;
import starling.utils.deg2rad;

public class Player {

    public static var perfects:Dictionary = new Dictionary();

    public function Player(perfect:PerfectObject, view:ObjectLayer) {
        this.perfect = perfect;
        layer = view;
        perfects[perfect] = this;

        body = new Sprite();
        body.visible = perfect.visible;
        body.addEventListener(TouchEvent.TOUCH, avatar_clickHandler);
        body.x = GeoUtils.lon2x(perfect.center.x);
        body.y = GeoUtils.lat2y(perfect.center.y);

        icon = SkyFactory.getPerfectIcon(perfect);
        body.addChild(icon);
        if (perfect.state == "drive") {
            icon.rotation = deg2rad(perfect.angle);
        }
        currentState = perfect.state;
        layer.add(body);
        perfect.geoUpdate = geoUpdate
    }

    public var currentState:String = "";
    public var body:Sprite;
    public var view:ObjectLayer;
    public var icon:Image;
    public var perfect:PerfectObject;
    public var layer:ObjectLayer;

    public function updateVisible():void {
        body.visible = perfect.visible;
    }

    private function geoUpdate():void {
        if (currentState != perfect.state) {
            body.removeChild(icon);
            icon.dispose();
            icon = SkyFactory.getPerfectIcon(perfect);
            body.addChild(icon);
        }

        currentState = perfect.state;
        if (perfect.state == "drive") {
            icon.rotation = deg2rad(perfect.angle);
        }
        TweenLite.to(body, 2, {x: GeoUtils.lon2x(perfect.center.x), y: GeoUtils.lat2y(perfect.center.y)});
    }

    private function avatar_clickHandler(event:TouchEvent):void {
        Core.call(PerfectEvents.PLAYER_SELECT, this);
    }

}
}
