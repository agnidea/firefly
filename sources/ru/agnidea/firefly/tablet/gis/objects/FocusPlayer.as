/**
 * Created by flower on 06.10.14.
 */
package ru.agnidea.firefly.tablet.gis.objects {

import ru.agnidea.firefly.tablet.gis.layers.ObjectLayer;
import ru.agnidea.utils.cmToPx;

import starling.core.Starling;
import starling.display.MovieClip;
import starling.display.Sprite;

public class FocusPlayer {
    public function FocusPlayer(view:ObjectLayer) {
        targetSprite = new Sprite()
        focus = SkyFactory.getSelect();
        layer = view;
        focus.loop = false;
        focus.pivotY = focus.pivotX = focus.width / 2;
        focus.width = cmToPx(1.4);
        focus.scaleY = focus.scaleX;
        focus.stop();
        focus.touchable = false;
        targetSprite.addChild(focus);
        targetSprite.touchable = false;
    }

    public var added:Boolean = false;
    private var targetSprite:Sprite;
    private var focus:MovieClip;
    private var layer:ObjectLayer;

    public function add():void {
        if (!added) {
            added = true;
            Starling.juggler.add(focus);
        }
    }


    public function to(angel:Player):void {
        if (!added) add();

        if (targetSprite.parent != angel.body) {
            if (targetSprite.parent) {
                targetSprite.parent.removeChild(targetSprite);
            }
            angel.body.addChild(targetSprite);
        }
        layer.setChildIndex(angel.body, angel.body.numChildren + 1);
//        targetSprite.x = angel.body.x;
//        targetSprite.y = angel.body.y;
        focus.currentFrame = 0;
        focus.play();
    }
}
}
