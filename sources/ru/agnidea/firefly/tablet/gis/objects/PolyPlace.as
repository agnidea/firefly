/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.tablet.gis.objects {
import flash.utils.Dictionary;

import ru.agnidea.firefly.Core;
import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.firefly.tablet.events.PerfectEvents;

import sk.yoz.ycanvas.map.display.MapLayer;
import sk.yoz.ycanvas.map.display.Polygon;
import sk.yoz.ycanvas.map.utils.OptimizedPointsUtils;
import sk.yoz.ycanvas.map.valueObjects.OptimizedPoints;

import starling.events.TouchEvent;

public class PolyPlace {
    public static var perfects:Dictionary = new Dictionary();

    public function PolyPlace(perfectObject:PerfectObject, view:MapLayer) {

        id = perfectObject.name;
        perfect = perfectObject;
        perfects[perfectObject] = this;

        var optimizedPoints:OptimizedPoints = OptimizedPointsUtils.calculate(perfectObject.geoVector);

        if (perfectObject.mainType == PerfectObject.LINE) {

        }
        poly = new Polygon(optimizedPoints.points, perfectObject.color, .4);

        poly.pivotX = optimizedPoints.pivotX;
        poly.pivotY = optimizedPoints.pivotY;
        poly.addEventListener(TouchEvent.TOUCH, clickHandler)
        view.addChild(poly);

        poly.visible = perfect.visible;
    }
    public var poly:Polygon;
    public var perfect:PerfectObject;
    public var id:String;

    public function select(b:Boolean):void {
        if (b) {
            poly.color = perfect.color;
            poly.alpha = 0.7;
        } else {
            poly.color = perfect.color;
            poly.alpha = 0.4;
        }
        poly.update();
    }

    private function clickHandler(event:*):void {
        Core.call(PerfectEvents.POLY_SELECT, this);
    }
}
}
