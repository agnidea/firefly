/**
 * Created by flower on 15.12.14.
 */
package ru.agnidea.firefly.tablet.gis.objects {
import flash.utils.Dictionary;

import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.utils.cmToPx;

import sk.yoz.ycanvas.map.display.MapStroke;
import sk.yoz.ycanvas.map.display.StrokeLayer;

public class TrackGis {
    public static var perfects:Dictionary = new Dictionary();

    public function TrackGis(perfectChild:PerfectObject, view:StrokeLayer) {
        stroke = new MapStroke(perfectChild.geoVector, cmToPx(0.2), perfectChild.color);
        stroke.visible = perfectChild.visible = perfectChild.color;
        perfects[perfectChild] = this;
        view.add(stroke);
    }

    public var stroke:MapStroke;
}
}
