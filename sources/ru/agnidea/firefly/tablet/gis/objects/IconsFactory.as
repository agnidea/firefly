/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.tablet.gis.objects {
import flash.display.BitmapData;
import flash.display.Sprite;

import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.utils.cmToPx;

public class IconsFactory {
    public static var spriteIcon:Sprite;

    private static var _icon:GfxIcons;

    private static function get icon():GfxIcons {
        if (!_icon) {
            _icon = new GfxIcons();
            spriteIcon = new Sprite();
            spriteIcon.addChild(_icon)
        }
        return _icon;
    }

    public static function get(perfect:PerfectObject):BitmapData {
        if (perfect.mainType == PerfectObject.PLAYER) {
            icon.gotoAndStop(perfect.state);
            var bw:int;
            var bh:int;
            if (perfect.state == "drive") {
                _icon.height = cmToPx(0.7);
                _icon.scaleX = _icon.scaleY;
                bh = spriteIcon.height;
                bw = spriteIcon.width;
            } else {
                _icon.height = cmToPx(0.5);
                _icon.scaleX = _icon.scaleY;
                _icon.y = _icon.x = 1;
                bh = spriteIcon.height + 2;
                bw = spriteIcon.width + 2;
            }
        } else {
            icon.gotoAndStop("ipad");
            _icon.height = cmToPx(0.7);
            _icon.scaleX = _icon.scaleY;
            _icon.y = _icon.x = 1;
            bh = spriteIcon.height + 2;
            bw = spriteIcon.width + 2;
        }
        var bitmapData:BitmapData = new BitmapData(bw, bh, true, 0x00000000);
        bitmapData.draw(spriteIcon);
        return bitmapData;
    }

    public function IconsFactory() {
        super();
    }
}
}
