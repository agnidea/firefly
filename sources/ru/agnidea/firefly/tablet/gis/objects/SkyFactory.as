/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.tablet.gis.objects {
import ru.agnidea.firefly.core.vo.PerfectObject;

import starling.display.Image;
import starling.display.MovieClip;
import starling.textures.Texture;
import starling.textures.TextureAtlas;

public class SkyFactory {
    [Embed(source="/spritesheets/selectAnimation.xml", mimeType="application/octet-stream")]
    private static const AtlasXml:Class;
    [Embed(source="/spritesheets/selectAnimation.png")]
    private static const AtlasTexture:Class;
    public static var imagesPool:Object = new Object();
    private static var texturesPool:Object = new Object();

    public static function getPerfectTexture(perfect:PerfectObject):Texture {
        if (!texturesPool[perfect.state]) {
            texturesPool[perfect.state] = Texture.fromBitmapData(IconsFactory.get(perfect));
        }
        return texturesPool[perfect.state];
//        return Texture.fromBitmapData(IconsFactory.get(perfect), false);
    }

    public static function getSelect():MovieClip {
        var texture:Texture = Texture.fromBitmap(new AtlasTexture());

        var xml:XML = XML(new AtlasXml());
        var atlas:TextureAtlas = new TextureAtlas(texture, xml);

        return new MovieClip(atlas.getTextures(), 33);
    }

    public static function getPerfectIcon(perfect:PerfectObject):Image {
//        if (!imagesPool[perfect.state]){
        var texture:Texture = getPerfectTexture(perfect);
        var image:Image = new Image(texture);
        image.pivotX = texture.width / 2;
        image.pivotY = texture.height / 2;
        return image;
//        }
//        return imagesPool[perfect.state];
    }

    public function SkyFactory() {
    }
}
}
