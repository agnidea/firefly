package ru.agnidea.firefly.tablet.gis {

import flash.events.Event;

import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.Core;
import ru.agnidea.firefly.core.AppState;
import ru.agnidea.firefly.core.vo.SettingsData;
import ru.agnidea.firefly.tablet.events.GisEvents;
import ru.agnidea.firefly.tablet.gis.layers.ObjectLayerMediator;
import ru.agnidea.firefly.tablet.gis.layers.PolygonLayerMediator;
import ru.agnidea.firefly.tablet.gis.layers.StrokeLayerMediator;

import sk.yoz.ycanvas.map.valueObjects.MapConfig;

import starling.core.Starling;

/**
 * @author flower
 */
public class YCMRootMediator extends Mediator {

    [Inject]
    public var view:YCMRoot;

    public var settings:SettingsData;

    override public function onRegister():void {
        mediatorMap.mediateWith(view.core.objectsLayer, ObjectLayerMediator);
        mediatorMap.mediateWith(view.core.polygonLayer, PolygonLayerMediator);
        mediatorMap.mediateWith(view.core.strokeLayer, StrokeLayerMediator);

        MapCore.init(view.core);
        addHandler(GisEvents.MAP_TYPE_CHANGE, mapTypeChange);
        settings = Core.settings.data;

        if (settings.autoSaveLocation) {
            Starling.current.nativeStage.addEventListener(Event.DEACTIVATE, onDeActivate);
        }

        if (settings.startYX) {
            view.core.transformation.centerX = settings.startX;
            view.core.transformation.centerY = settings.startY;
        }
        if (settings.startZoom) view.core.transformation.scale = settings.startZoom;

        addHandler(GisEvents.HIDE, onHideGis);
        addHandler(GisEvents.FREEZE, onFreezeGis);

    }

    private function onFreezeGis(value:Boolean):void {
//        AppState.freezeGis = value;
        Starling.current.root.touchable = !value;
        view.core.transformationManager.allowMove = !value;
        view.core.transformationManager.allowZoom = !value;
    }

    private function onHideGis(value:Boolean):void {
        AppState.hideGis = value;
        if (value) {
            Starling.current.stop();
            Starling.current.root.touchable = false;
            view.removeGis();
        } else {
            Starling.current.start();
            Starling.current.root.touchable = true;
            view.addGis();
        }
    }

    private function mapTypeChange(data:MapConfig):void {
        view.core.map.config = data;
    }

    private function onDeActivate(event:Event):void {
        Core.settings.saveLocationAndZoom(view.core.map.center, view.core.map.scale);
    }

}
}