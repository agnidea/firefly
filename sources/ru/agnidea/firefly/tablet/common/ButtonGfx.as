/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.tablet.common {
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.MovieClip;
import flash.events.MouseEvent;

import mx.core.UIComponent;

public class ButtonGfx extends UIComponent {

    public function ButtonGfx() {
        bitmap = new Bitmap(new BitmapData(1, 1));
        addChild(bitmap);
    }

    public var gfx:MovieClip;
    public var normal:BitmapData;
    public var down:BitmapData;
    public var selected:BitmapData;
    public var active:Boolean = false;
    public var gfxAdditional:MovieClip;
    public var icon:MovieClip;
    private var bitmap:Bitmap;

    public function set gfxClass(gfxClass:Class):void {
        if (!gfx) {
            gfx = new gfxClass;
            this.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
            this.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
            mouseEnabled = true;
        }
    }

    override public function setLayoutBoundsSize(width:Number, height:Number, postLayoutTransform:Boolean = true):void {
        super.setLayoutBoundsSize(width, height, postLayoutTransform);

        if (width) this.width = width;
        if (height) this.height = height;
        if (!gfx) return;
        if (this.width == 0 && !width) return;
        if (!normal && this.width && this.height) {
            drawSprites();
        } else if (normal.width != this.width || normal.height != this.height) {
            drawSprites();
        }
    }

    public function drawSprites():void {
        gfx.bg.height = this.height;
        gfx.bg.width = this.width;
        if (gfxAdditional) {
            gfxAdditional.width = this.width;
            gfxAdditional.height = this.height;
        }
        normal = new BitmapData(width, height, true, 0x00000000);
        selected = normal.clone();
        down = normal.clone();
        gfx.bg.gotoAndStop(1);
        normal.draw(gfx);
        if (icon) icon.gotoAndStop(2);
        gfx.bg.gotoAndStop(2);
        down.draw(gfx);
        if (icon) icon.gotoAndStop(2);
        gfx.bg.gotoAndStop(3);
        selected.draw(gfx);
//        bitmap.bitmapData = normal;
        setBitmapData(normal);
    }

    public function switchActive():Boolean {
        active = !active;
        updateActive();
        return active;
    }

    public function updateActive():void {
        if (active) {
            setBitmapData(selected);
        } else {
            setBitmapData(normal);
        }
    }

    private function setBitmapData(normal:BitmapData):void {
        bitmap.bitmapData = normal;
    }

    private function mouseUpHandler(event:MouseEvent):void {
        updateActive();
    }

    private function mouseDownHandler(event:MouseEvent):void {
        setBitmapData(down);
    }
}
}
