/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.tablet.events {
public class PerfectEvents {

    public static const CURRENT_PLAYER_CHANGE:String = "current_player_change";
    public static const REFRESH_PLAYERS:String = "refresh_players";
    public static const PLAYERS_LOADED:String = "players_loaded";
    public static const PLAYER_SELECT:String = "player_select";

    public static const POLY_SELECT:String = "poly_select";
    public static const REFRESH_PLACES:String = "refresh_places";
    public static const PLACES_LOADED:String = "places_loaded";

    public static const PERFECT_SELECT:String = "perfect_select";
    public static const PERFECT_CHANGE:String = "perfect_change";
    public static const SOUL_INFO:String = "soul_info";
    public static const PERFECT_SHOW:String = "perfect_show";
    public static const PERFECT_HIDE:String = "perfect_hide";

    public function PerfectEvents() {

    }
}
}
