/**
 * Created by flower on 15.10.14.
 */
package ru.agnidea.firefly.tablet.events {
public class GisEvents {
    public static const MAP_TYPE_CHANGE:String = "map_type_change";
    public static const HIDE:String = "gis_hide";
    public static const FREEZE:String = "gis_freeze";

    public function GisEvents() {

    }

}
}
