/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.tablet.events {
public class GuiEvents {

    public static const SHOW_SETTINGS:String = "show_settings";
    public static const SHOW_CHAT:String = "show_chat";
    public static const SHOW_MAIN:String = "show_main";
    public static const SHOW_EDIT:String = "show_edit";
    public static const NEED_UPDATE:String = "need_update";
    public static const INFOCARD_CLOSE:String = "info_card_close";
    public static const SHOW_UPDATE:String = "show_update";
    public static const SHOW_ERROR:String = "show_error";

    public function GuiEvents() {
    }
}
}
