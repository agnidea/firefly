/**
 * Created by flower on 15.12.14.
 */
package ru.agnidea.firefly.tablet.events {
public class DataEvents {
    public static const CATALOG_GEO_UPDATE:String = "catalog_data_update";
    public static const IMPORT_KML:String = "import_kml";

    public function DataEvents() {
    }
}
}
