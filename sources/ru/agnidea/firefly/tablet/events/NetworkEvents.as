/**
 * Created by flower on 17.11.14.
 */
package ru.agnidea.firefly.tablet.events {
public class NetworkEvents {
    public static const GMS_ERROR:String = "gms_error";
    public static const LOGIN:String = "net_login";
    public static const LOGIN_SUCCESS:String = "net_login_success";
    public static const GMS_UPDATE:String = "gms_update";
    public static var GPS_UPDTATE:String = "gps_update";

    public function NetworkEvents() {
    }
}
}
