package ru.agnidea.firefly.tablet.chat.commands {
import org.mvcexpress.mvc.Command;

import ru.agnidea.firefly.tablet.chat.model.ChatEvents;
import ru.agnidea.firefly.tablet.chat.model.MessagesProxy;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class ChatGetHistory extends Command {

    [Inject]
    public var messagesProxy:MessagesProxy;

    public function execute(force:Boolean = false):void {
        if (messagesProxy.messagesHistory) {
            sendMessage(ChatEvents.ONGET_HISTORY, messagesProxy.messagesHistory);
            if (force) messagesProxy.updateMesages();
        } else {
            messagesProxy.updateMesages();
        }
    }

}
}
