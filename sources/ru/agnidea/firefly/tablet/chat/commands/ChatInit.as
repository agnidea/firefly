package ru.agnidea.firefly.tablet.chat.commands {
import org.mvcexpress.mvc.Command;

import ru.agnidea.firefly.tablet.chat.model.ChatEvents;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class ChatInit extends Command {

    public function execute(a:Object = null):void {
        commandMap.map(ChatEvents.GET_HISTORY, ChatGetHistory);
        commandMap.map(ChatEvents.SEND_MESSAGE, ChatSendMessage);
        commandMap.map(ChatEvents.SEND_PERFECT_UPDATE, SendPerfectUpdate);
        commandMap.map(ChatEvents.UPDATE_PERFECT, PerfectUpdate);
    }

}
}
