package ru.agnidea.firefly.tablet.chat.commands {
import org.mvcexpress.mvc.Command;

import ru.agnidea.firefly.core.P2PProxy;
import ru.agnidea.firefly.tablet.chat.model.MessageVO;
import ru.agnidea.firefly.tablet.chat.model.MessagesProxy;
import ru.agnidea.firefly.tablet.chat.model.UpdateMessageGenerator;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class SendPerfectUpdate extends Command {
    [Inject]
    public var p2p:P2PProxy;
    [Inject]
    public var messagesProxy:MessagesProxy;

    public function execute(umg:UpdateMessageGenerator):void {
        var message:MessageVO = messagesProxy.sendLogMessage(umg.generatedMessage);
        message.data = new Object();
        message.data.uid = umg.perfectObject.uid;
        message.data.id = umg.perfectObject.id;
        message.data.perfectO = umg.perfectO;
        if (!p2p.service.connected) {
            p2p.sendOnConnect = message.toString();
            p2p.service.connect();
            return;
        }
//        sendMessage(ChatEvents.NEW_MESSAGE, message);
        p2p.sendString(message.toString());

        message.saveQueued();
    }

}
}
