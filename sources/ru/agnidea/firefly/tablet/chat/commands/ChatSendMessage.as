package ru.agnidea.firefly.tablet.chat.commands {
import org.mvcexpress.mvc.Command;

import ru.agnidea.firefly.core.P2PProxy;
import ru.agnidea.firefly.tablet.chat.model.ChatEvents;
import ru.agnidea.firefly.tablet.chat.model.MessageVO;
import ru.agnidea.firefly.tablet.chat.model.MessagesProxy;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class ChatSendMessage extends Command {

    [Inject] public var messagesProxy:MessagesProxy;
    [Inject] public var p2p:P2PProxy;

    public function execute(messageText:String):void {
        if (!p2p.service.connected) {
            p2p.service.connect();
            sendMessage(ChatEvents.GET_HISTORY);
            return;
        }
        var message:MessageVO = messagesProxy.sendChatMessage(messageText);
        message.saveQueued();
        sendMessage(ChatEvents.NEW_MESSAGE, message);
        p2p.sendString(message.toString());
    }

}
}
