package ru.agnidea.firefly.tablet.chat {
import flash.events.MouseEvent;

import org.apache.flex.collections.ArrayList;
import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.firefly.tablet.chat.model.ChatEvents;
import ru.agnidea.firefly.tablet.chat.model.MessageVO;
import ru.agnidea.firefly.tablet.chat.view.*;
import ru.agnidea.firefly.tablet.events.GuiEvents;
import ru.agnidea.firefly.tablet.events.PerfectEvents;

import spark.events.IndexChangeEvent;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class ChatViewMediator extends Mediator {

    [Inject]
    public var view:ChatView;

    override public function onRegister():void {
        addHandler(ChatEvents.ONGET_HISTORY, onChatHistory);
        addHandler(ChatEvents.NEW_MESSAGE, onNewMessage);
        sendMessage(ChatEvents.GET_HISTORY);

        view.backButton.addEventListener(MouseEvent.CLICK, backClickHandler);
        view.messagesView.sendButton.addEventListener(MouseEvent.CLICK, sendClickHandler);
        view.messagesView.list.addEventListener(IndexChangeEvent.CHANGE, messageSelectChangeHandler);
    }

    override public function onRemove():void {
    }

    private function onNewMessage(newMessage:MessageVO):void {
        view.messagesView.list.dataProvider.addItem(newMessage);
        view.messagesView.downScroll();
    }

    private function onChatHistory(ar:Array):void {
        view.messagesView.list.dataProvider = new ArrayList(ar);
        view.messagesView.downScroll();
    }

    private function backClickHandler(event:MouseEvent):void {
        sendMessage(GuiEvents.SHOW_MAIN);
    }

    private function sendClickHandler(event:MouseEvent):void {
        if (!view.messagesView.textInput.text.length) return;
        view.stage.focus = view.messagesView.textInput;
        sendMessage(ChatEvents.SEND_MESSAGE, view.messagesView.textInput.text);
        view.stage.focus = view.messagesView.textInput;
        view.messagesView.textInput.text = "";
    }


    private function messageSelectChangeHandler(event:IndexChangeEvent):void {
        var msg:MessageVO = view.messagesView.list.selectedItem as MessageVO;
        if (msg.mainType == MessageVO.EVENT) {
            var perfectObject:PerfectObject = new PerfectObject(PerfectObject.SOUL);
            perfectObject.name = msg.senderName;
            perfectObject.uid = msg.id;
            perfectObject.updateCenter(msg.data.x, msg.data.y);
            perfectObject.date = msg.createdAt;
            perfectObject.data.info = msg.senderInfo;
            sendMessage(PerfectEvents.SOUL_INFO, perfectObject);
        } else if (msg.data) {
//            if (msg.data.hardName) {
//                var po:PerfectObject = PerfectObject.gethard(msg.data.hardName);
//                if (po) sendMessage(PerfectEvents.PERFECT_CHANGE, po)
//            }
        } else {
            sendMessage(GuiEvents.INFOCARD_CLOSE);
        }

    }
}
}