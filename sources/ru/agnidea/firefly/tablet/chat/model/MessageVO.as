/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.tablet.chat.model {
import com.gamua.flox.Access;
import com.gamua.flox.Entity;

public class MessageVO extends Entity{
    public static var EVENT:String = 'event';
    public static var MESSAGE:String = 'message';
//    public var user:String;
    public static var LOG:String = 'log';

    public function MessageVO() {
        publicAccess = Access.READ_WRITE;
    }

    public var data:Object;
    public var text:String;
    public var timeInt:int;
    public var senderDID:String;
    public var senderName:String = "";
    public var senderInfo:String = "";
    public var mainType:String;
    public var attachmentsObjectsIDs:String;

    private var _time:int;

    public function get time():Number {
        if (!_time) {
            _time = createdAt.time;
        }
        return _time;
    }

    override public function toString():String {
         return JSON.stringify(this);
    }

    public function fromString(s:String):void {
        var o:Object = JSON.parse(s);
        data = o.data;
        text = o.text;
        timeInt = o.timeInt;
        senderName = o.senderName;
        mainType = o.mainType;
    }
}
}
