/**
 * Created by flower on 16.11.14.
 */
package ru.agnidea.firefly.tablet.chat.model {
public class ChatEvents {
    public static const GET_HISTORY:String = "chat_get_history";
    public static const ONGET_HISTORY:String = "chat_on_get_history";
    public static const SEND_MESSAGE:String = "chat_send_message";
    public static const NEW_MESSAGE:String = "chat_new_messages";
    public static const SEND_PERFECT_UPDATE:String = "send_perfect_update";
    public static const UPDATE_PERFECT:String = "update_perfect";

    public function ChatEvents() {
    }
}
}
