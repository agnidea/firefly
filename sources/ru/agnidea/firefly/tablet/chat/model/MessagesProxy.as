package ru.agnidea.firefly.tablet.chat.model {
import com.gamua.flox.Query;

import org.mvcexpress.mvc.Proxy;

import ru.agnidea.firefly.core.vo.User;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class MessagesProxy extends Proxy {

    public function MessagesProxy() {
    }

    public var messagesHistory:Array = [];

    override protected function onRegister():void {



    }

    override protected function onRemove():void {
    }

    public function updateMesages():void {
        var query:Query = new Query(MessageVO);
        query.limit = 20;
        query.orderBy = "createdAt DESC";
        query.find(
                function onComplete(ar:Array):void {
                    ar.reverse();
                    messagesHistory = ar;
                    sendMessage(ChatEvents.ONGET_HISTORY, messagesHistory);
                },
                function onError(error:String):void {
                    //Something went wrong during the execution of the query.
                    //The player's device may be offline.
                }
        );
    };

    public function sendChatMessage(messageText:String):MessageVO {
        var message:MessageVO = new MessageVO();
        message.senderName = User.login;
        message.senderInfo = User.info;
        message.senderDID = User.deviceID;
        message.text = messageText;
        message.mainType = MessageVO.MESSAGE;
        message.timeInt = new Date().getTime();
        return message;
    }


    public function sendLogMessage(generatedMessage:String):MessageVO {
        var message:MessageVO = new MessageVO();
        message.senderName = User.login;
        message.senderInfo = User.info;
        message.senderDID = User.deviceID;
        message.text = generatedMessage;
        message.mainType = MessageVO.LOG;
        message.timeInt = new Date().getTime();
        messagesHistory.push(message);
        return message;
    }
}
}