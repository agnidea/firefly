/**
 * Created by flower on 28.11.14.
 */
package ru.agnidea.firefly.tablet.chat.model {
import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.firefly.core.vo.User;
import ru.agnidea.utils.DateFire;

public class UpdateMessageGenerator {
    public function UpdateMessageGenerator() {
    }

    public var perfectObject:PerfectObject;
    private var name:Boolean
    private var oldName:String;
    private var startDate:Boolean;
    private var endDate:Boolean;
    private var cultureID:Boolean;
    private var transportCatID:Boolean;
    private var transportGosNumber:Boolean;

    public function get generatedMessage():String {
        var type:String = "";
        if (perfectObject.mainType == PerfectObject.PLACE) {
            type = "поля";
        } else {
            type = "объекта";
        }
        var gm:String = "Пользователь " + User.login + " ";
        if (name) gm += "переименовал '" + oldName + "' в '" + perfectObject.name + "'";
        else gm += " изменил значения " + type + " '" + perfectObject.name + "':";

        if (cultureID) gm += " Изменена культура '" + perfectObject.getCultureClass().name + "'.";
        if (startDate) gm += " Изменена дата посева '" + DateFire.dateRu(perfectObject.startDate) + "'.";
        if (endDate) gm += " Изменена дата сбора урожая '" + DateFire.dateRu(perfectObject.endDate) + "'.";

        if (transportCatID) gm += " Изменена категория транспортного средства '" + perfectObject.getTransportCatecoryClass().name + "'.";
        if (transportGosNumber) gm += " Измененён государственный номер '" + perfectObject.transportGosNumber + "'.";
        return gm;
    }

    public function get perfectO():Object {
        var o:Object = {};
        if (name) o.name = perfectObject.name;
        if (cultureID) o.cultureID = perfectObject.cultureID;
        if (startDate) o.startDate = perfectObject.startDate.getTime();
        if (endDate) o.endDate = perfectObject.endDate.getTime();
        if (transportCatID) o.transportCatID = perfectObject.transportCatID;
        if (transportGosNumber) o.transportGosNumber = perfectObject.transportGosNumber;
        return o;
    }

    public function checkName(old:String, nw:String):void {
        if (old != nw) {
            name = true;
            oldName = old;
        }
    }

    public function checkStarDate(old:Date, selectedDate:Date):void {
        if (!old) {
            startDate = true;
            return;
        }
        if (old.toDateString() != selectedDate.toDateString()) {
            startDate = true;
        }
    }

    public function checEndDate(old:Date, selectedDate:Date):void {
        if (!old) {
            endDate = true;
            return;
        }
        if (old.toDateString() != selectedDate.toDateString()) {
            endDate = true;
        }
    }

    public function checkCultureID(old:int, id:int):void {
        if (old != id) {
            cultureID = true;
        }
    }

    public function checkTransportCatID(old:int, id:int):void {
        if (old != id) {
            transportCatID = true;
        }
    }

    public function checkTransportGosNumber(old:String, id:String):void {
        if (old != id) {
            transportGosNumber = true;
        }
    }
}
}
