/**
 * Created by flower on 12.11.14.
 */
package ru.agnidea.firefly.tablet.chat.view {
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

import flashx.textLayout.formats.TextAlign;

import ru.agnidea.firefly.core.vo.User;
import ru.agnidea.firefly.tablet.chat.model.MessageVO;
import ru.agnidea.firefly.tablet.common.ItemRendererBase;
import ru.agnidea.utils.DateFire;
import ru.agnidea.utils.adaptFontSizeToCurDPI;
import ru.agnidea.utils.cmToPx;

public class OneMessageRenderer extends ItemRendererBase {
    public function OneMessageRenderer() {
        avatar = new GfxMessageAvatar();
        avatar.width = cmToPx(1.3);
        avatar.scaleY = avatar.scaleX;
        addChild(avatar);

        gfxMessage = new GfxMessageBox();
        gfxMessage.x = avatar.width + cmToPx(0.1);
        addChild(gfxMessage);

        var tf:TextFormat = gfxMessage.mainText.getTextFormat();
        tf.size = adaptFontSizeToCurDPI(28);
        gfxMessage.mainText.autoSize = TextFieldAutoSize.LEFT;
        gfxMessage.mainText.wordWrap = true;
        gfxMessage.mainText.defaultTextFormat = tf;

//        gfxMessage.mainText.background = true;
//        gfxMessage.mainText.backgroundColor = 0xFFFFFF;
        gfxMessage.mainText.x = cmToPx(0.1);
        gfxMessage.bg.selected.visible = false;


        tf.color = 0xB3E5FC;//0xB3E5FC;
        tf.size = adaptFontSizeToCurDPI(22);
        dateText.defaultTextFormat = tf;
        addChild(dateText);

    }

    public var dateText:TextField = new TextField();
    public var avatar:GfxMessageAvatar;
    public var gfxMessage:GfxMessageBox;
    public var message:MessageVO;
    private var lastSet:String;
    private var tf:TextFormat;

    override public function set data(value:Object):void {
        super.data = value;
        if (data is MessageVO) message = data as MessageVO;

        if (message.mainType == MessageVO.EVENT) {
            avatar.gotoAndStop(3);
            gfxMessage.bg.gotoAndStop(3);
        } else {
            if (message.senderName == User.login) {
                avatar.gotoAndStop(2);
                gfxMessage.bg.gotoAndStop(2);
            } else {
                avatar.gotoAndStop(1);
                gfxMessage.bg.gotoAndStop(1);
            }
            avatar.loginText.text = message.senderName;
//            avatar.infoText.text = message.senderInfo;
        }
        gfxMessage.mainText.text = message.text;

        dateText.text = DateFire.time(message.createdAt) + " \n" + DateFire.dateRu(message.createdAt);
//        if (lastSet != message.senderName);
        updatePos();
    }

    override public function set selected(value:Boolean):void {
        super.selected = value;
        gfxMessage.bg.selected.visible = value;
    }

    override public function setLayoutBoundsSize(width:Number, height:Number, postLayoutTransform:Boolean = true):void {
        super.setLayoutBoundsSize(width, height, postLayoutTransform);
//        avatar.width = width;
//        avatar.scaleY = avatar.scaleX;

        gfxMessage.mainText.y = cmToPx(0.1);
        if (message.senderName == User.login) {
            avatar.x = width - cmToPx(1.3);
            gfxMessage.x = cmToPx(1.9);
            dateText.x = 0;
            dateText.width = gfxMessage.x - cmToPx(0.1);
            tf = dateText.getTextFormat();
            tf.align = TextAlign.RIGHT;
            dateText.defaultTextFormat = tf;
            dateText.setTextFormat(tf);
        } else {

            tf = dateText.getTextFormat();
            tf.align = TextAlign.LEFT;
            dateText.defaultTextFormat = tf;
            dateText.setTextFormat(tf);
            gfxMessage.x = cmToPx(1.4);
            avatar.x = 0;
            dateText.x = gfxMessage.width + gfxMessage.x + cmToPx(0.1);
        }

        gfxMessage.bg.width = width - cmToPx(3.3);
        gfxMessage.mainText.width = gfxMessage.bg.width - cmToPx(0.3);
        gfxMessage.bg.height = gfxMessage.mainText.textHeight + cmToPx(0.3);

        this.height = gfxMessage.bg.height + cmToPx(0.1);

        dateText.y = height / 2;
        if (this.height < avatar.height) {
            this.height = avatar.height + cmToPx(0.1);
        }
    }

    private function updatePos():void {
        lastSet = message.senderName;
        invalidateDisplayList();
    }
}
}
