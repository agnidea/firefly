/**
 * Created by flower on 23.10.14.
 */
package ru.agnidea.firefly.tablet.view.settings {

import flash.events.Event;
import flash.events.MouseEvent;

import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.Core;
import ru.agnidea.firefly.core.vo.SettingsData;
import ru.agnidea.firefly.tablet.events.GisEvents;
import ru.agnidea.firefly.tablet.events.GuiEvents;

import sk.yoz.ycanvas.map.valueObjects.MapConfig;

import spark.components.RadioButton;

public class SettingsMediator extends Mediator {


    [Inject]
    public var view:SettingsView;
    public var settings:SettingsData;
    private var selectedMap:MapConfig;
    private var last:MapConfig;

    override public function onRegister():void {
        super.onRegister();
        view.cancelButton.addEventListener(MouseEvent.CLICK, cancelHandler);
        view.saveButton.addEventListener(MouseEvent.CLICK, saveHandler);
        view.addEventListener(Event.ADDED_TO_STAGE, view_addedToStageHandler);
        view.changeEventFn = mapTypeSwitch;
        settings = Core.settings.data;
        init();
    }

    override public function onRemove():void {
        super.onRemove();
    }

    public function init():void {
        if (settings.currentMapType) {
            var rb:RadioButton = view[settings.currentMapType];
            rb.selected = true;
        }
        if (settings.currentMapType) {
            var rb:RadioButton = view[settings.currentMapType];
            rb.selected = true;
        }
        if (settings.autoSaveLocation) {
            view.autoSaveCheckBox.selected = settings.autoSaveLocation;
            last = settings.getCurrentMapType();
        }
    }

    private function mapTypeSwitch(data:MapConfig):void {
        selectedMap = data;
        sendMessage(GisEvents.MAP_TYPE_CHANGE, data);
    }

    private function view_addedToStageHandler(event:Event):void {
        init();
    }

    private function cancelHandler(event:MouseEvent):void {
        sendMessage(GuiEvents.SHOW_SETTINGS, false);
        if (last)
            sendMessage(GisEvents.MAP_TYPE_CHANGE, last);
    }

    private function saveHandler(event:MouseEvent):void {
        sendMessage(GuiEvents.SHOW_SETTINGS, false);
        Core.settings.saveMapType(selectedMap);
    }


}
}
