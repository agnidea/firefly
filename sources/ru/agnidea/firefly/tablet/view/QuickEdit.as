/**
 * Created by flower on 26.12.14.
 */
package ru.agnidea.firefly.tablet.view {
import flash.events.MouseEvent;

import mx.core.UIComponent;

import spark.components.Application;

public class QuickEdit {
    public static var field:QuickEdit;
    private static var con:Application;

    public static function init(group:Application):void {
        con = group;
        if (!field) field = new QuickEdit();
    }

    public function QuickEdit() {

    }


    private var ef:EditFieldGroup = new EditFieldGroup();
    private var ui:UIComponent = new UIComponent();
    private var complete:Function;

    public function start(value:String, title:String, onEditComplete:Function):void {
        ef.finishButton.addEventListener(MouseEvent.CLICK, clickHandler);
        ef.cancelButton.addEventListener(MouseEvent.CLICK, cancelHandler);
        ef.textInput.text = value;
        ef.titleLabel.text = title;
        ui = new UIComponent();
        con.addElement(ef);
        complete = onEditComplete;
    }

    private function cancelHandler(event:MouseEvent):void {
        con.removeElement(ef);
    }

    private function clickHandler(event:MouseEvent):void {
        con.removeElement(ef);
        if (complete != null) complete(ef.textInput.text);

    }


}
}
