package ru.agnidea.firefly.tablet.view.main {
import flash.events.Event;
import flash.events.MouseEvent;

import mx.events.FlexEvent;

import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.core.AppState;
import ru.agnidea.firefly.tablet.chat.model.ChatEvents;
import ru.agnidea.firefly.tablet.events.GisEvents;
import ru.agnidea.firefly.tablet.events.GuiEvents;
import ru.agnidea.firefly.tablet.view.main.bars.OtherBarMediator;
import ru.agnidea.firefly.tablet.view.main.bars.PlacesMediator;
import ru.agnidea.firefly.tablet.view.main.bars.PlayersMediator;
import ru.agnidea.firefly.tablet.view.settings.SettingsMediator;
import ru.agnidea.firefly.tablet.view.settings.SettingsView;

public class MobileViewMediator extends Mediator {

    [Inject]
    public var view:MobileView;
    private var settingsView:SettingsView;

    override public function onRegister():void {

        view.addEventListener(FlexEvent.CREATION_COMPLETE, view_creationCompleteHandler);

//        addHandler(GUIEvents.LEFT_BUTTON_CLICK, leftShow);
//        addHandler(GUIEvents.RIGHT_BUTTON_CLICK, rightShow);
        view.perfectLeft.mouseEnabled = false;
        view.perfectRight.mouseEnabled = false;

        view.addEventListener(MouseEvent.MOUSE_DOWN, gui_hitDownHandler);
        view.addEventListener(MouseEvent.MOUSE_UP, gui_hitUpHandler);

        addHandler(GuiEvents.SHOW_SETTINGS, onSettingsShow);
        addHandler(ChatEvents.NEW_MESSAGE, onNewMessage)

        var infoCard:GfxObjectCard = new GfxObjectCard();
        mediatorMap.mediateWith(infoCard, InfoCardMediator);
    }


    private function onNewMessage(o:* = null):void {
        if (AppState.infoCardState == "chat") return;
        AppState.unreadMessges++;
//        view.otherBars.downMenu.showCount(AppState.unreadMessges);
    }

    private function onSettingsShow(valus:Boolean):void {
        if (valus) {
            if (!settingsView) {
                settingsView = new SettingsView();
                mediatorMap.mediateWith(settingsView, SettingsMediator);
            }
            view.addElement(settingsView);
        } else {
            if (settingsView)
                if (view.getElementIndex(settingsView) > 0)
                    view.removeElement(settingsView);
        }
    }

    private function gui_hitDownHandler(event:Event):void {
        if (event.target is MobileView) return;
        sendMessage(GisEvents.FREEZE, true);
        event.stopImmediatePropagation();
        event.stopPropagation();
    }

    private function gui_hitUpHandler(event:MouseEvent):void {
        if (event.target is MobileView) return;
        sendMessage(GisEvents.FREEZE, false);
//        sendMessage(GuiEvents.INFOCARD_CLOSE);
//        event.stopImmediatePropagation();
//        event.stopPropagation();
    }

//    private function leftShow(value:Boolean):void {
//        view.perfectLeft.visible = value;
//        view.perfectLeft.mouseEnabled = value;
//    }
//
//    private function rightShow(value:Boolean):void {
//        view.perfectRight.visible = value;
//        view.perfectRight.mouseEnabled = value;
//    }

    private function view_creationCompleteHandler(event:FlexEvent):void {
        mediatorMap.mediateWith(view.otherBars, OtherBarMediator);
        mediatorMap.mediateWith(view.perfectLeft, PlacesMediator);
        mediatorMap.mediateWith(view.perfectRight, PlayersMediator);
    }
}
}