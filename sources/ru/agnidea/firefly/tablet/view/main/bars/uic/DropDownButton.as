/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.tablet.view.main.bars.uic {

import flash.system.Capabilities;
import flash.text.TextField;
import flash.text.TextFormat;

import ru.agnidea.firefly.tablet.common.ButtonGfx;
import ru.agnidea.utils.cmToPx;

public class DropDownButton extends ButtonGfx {

    public function DropDownButton() {

    }

    private var textField:TextField;
    private var mc:GfxDropDown;

    override public function set gfxClass(gfxClass:Class):void {
        if (gfx is gfxClass) return;
        super.gfxClass = GfxDropDown;
        mc = gfx as GfxDropDown;
        icon = mc.icon;
        if (_angel == "left") {
            gfxAdditional = mc.lbg;
            mc.removeChild(mc.rbg)
        } else {
            gfxAdditional = mc.rbg;
            mc.removeChild(mc.lbg)
        }
        initLabel();
    }

    private var _angel:String;

    public function set angel(s:String):void {
        _angel = s;
    }

    override public function drawSprites():void {
        gfxAdditional.height = this.height;
        gfxAdditional.width = 40;
        mc.icon.x = Capabilities.screenDPI > 140 ? cmToPx(.3) : cmToPx(.35);
        mc.icon.y = height / 2;
        mc.icon.height = Capabilities.screenDPI > 140 ? height / 3 : height / 4;
        mc.icon.scaleX = mc.icon.scaleY;
        if (_angel == "left") {
            mc.icon.x = Capabilities.screenDPI > 140 ? this.width - cmToPx(.3) : this.width - cmToPx(.35);
            textField.x = cmToPx(0.01);
        }

        textField.x = cmToPx(0.5);
        textField.y = cmToPx(0.15);
        textField.width = width - cmToPx(0.4);
        super.drawSprites();

    }

    public function setLabel(s:String):void {
        var tf:TextFormat = textField.getTextFormat();
        tf.size = cmToPx(0.25);
        textField.defaultTextFormat = tf;
//        textField.x = cmToPx(0.5);
//        textField.y = cmToPx(0.15);
//        textField.width = width - cmToPx(0.4);
        textField.text = s;
    }

    private function initLabel():void {
        if (textField) return;
        textField = gfx.label;
        gfx.removeChild(textField);
        addChild(textField);

    }
}
}
