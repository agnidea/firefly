/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.tablet.view.main.bars.uic {
import flash.display.MovieClip;

import mx.core.UIComponent;

import ru.agnidea.utils.cmToPx;

public class OpenButton extends UIComponent {

    public function OpenButton() {
    }


    public var gfx:MovieClip;

    public function set gfxClass(gfxClass:Class):void {
        if (!gfx) {
            gfx = new gfxClass;
//            this.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
//            this.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
//            mouseEnabled = true;
            this.width = gfx.width = cmToPx(0.8);
            gfx.scaleY = gfx.scaleX
            this.height = gfx.height;
            addChild(gfx);
        }
    }


}
}
