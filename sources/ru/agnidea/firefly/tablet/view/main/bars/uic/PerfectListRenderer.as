/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.tablet.view.main.bars.uic {
import flash.events.MouseEvent;
import flash.text.TextFormat;

import ru.agnidea.firefly.Core;
import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.firefly.tablet.common.ItemRendererBase;
import ru.agnidea.firefly.tablet.events.PerfectEvents;

public class PerfectListRenderer extends ItemRendererBase {
    public function PerfectListRenderer() {
        gfx = new GfxPerfectItem();
        addChild(gfx);
        gfx.selected.visible = false;
        gfx.active.gotoAndStop(1);
        gfx.active.addEventListener(MouseEvent.CLICK, mouseFocusChangeHandler);
        tf = gfx.label.getTextFormat();
    }

    public var gfx:GfxPerfectItem;


    private var tf:TextFormat;
    private var active:Boolean = false;


    private var perfect:PerfectObject;
    override public function set data(value:Object):void {
        super.data = value;

        perfect = value as PerfectObject;
        gfx.label.text = value.label;
//        if (active != perfect.visible){
        active = perfect.visible;
        updateActive();
//        }
        if (perfect.mainType == PerfectObject.POLY) {
//            gfx.type.text = perfetc.getCultureClass().name;
        } else {
//            gfx.type.text = value.getTransportCatecoryClass().name;
        }

    }

    override public function set selected(value:Boolean):void {
        super.selected = value;
        tf.color = value ? 0x81D4FA : 0xE1F5FE;
        gfx.label.setTextFormat(tf);
        gfx.selected.visible = value;
    }

    override public function setLayoutBoundsSize(width:Number, height:Number, postLayoutTransform:Boolean = true):void {
        super.setLayoutBoundsSize(width, height, postLayoutTransform);
        gfx.width = width;
        gfx.scaleY = gfx.scaleX;
        this.height = gfx.height;
    }

    private function updateActive():void {
        if (active) {
            gfx.active.gotoAndStop(1);
        } else {
            gfx.active.gotoAndStop(2);
        }
    }

    private function mouseFocusChangeHandler(event:*):void {
        active = !active;
        data.visible = active;
        updateActive();
        perfect.saveQueued();
        Core.call(PerfectEvents.PERFECT_HIDE, data);
        if (Core.data.catalog.movingObjects.getItemById(data.id)) {
            Core.data.catalog.movingObjects.store();
            trace(Core.data.catalog.movingObjects.getItemById(data.id).visible, data.id);
        }

        if (Core.data.catalog.geoObjects.getItemById(data.id)) {
            Core.data.catalog.geoObjects.store();

        }
    }
}
}
