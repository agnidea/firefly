/**
 * Created by flower on 26.11.14.
 */
package ru.agnidea.firefly.tablet.view.main.bars.uic {
import mx.core.UIComponent;

public class PanelsTabBar extends UIComponent {

    public function PanelsTabBar() {
        super();
        gfx = new GfxTabBar();
        gfx.stop();
//        gfx.bt1.icon.gotoAndStop(1);
//        gfx.bt2.icon.gotoAndStop(2);
//        gfx.bt3.icon.gotoAndStop(3);
        addChild(gfx);

    }

    public var gfx:GfxTabBar;

    override public function setLayoutBoundsSize(width:Number, height:Number, postLayoutTransform:Boolean = true):void {
        super.setLayoutBoundsSize(width, height, postLayoutTransform);

        if (width) this.width = width;
        if (height) this.height = height;
        if (!gfx || this.width == 0) return;

        gfx.width = this.width + 1;
        gfx.scaleY = gfx.scaleX;
        this.height = gfx.height - 1;

    }
}
}
