package ru.agnidea.firefly.tablet.view.main.bars {
import flash.events.MouseEvent;

import mx.events.FlexEvent;

import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.firefly.tablet.events.PerfectEvents;
import ru.agnidea.utils.DataCollection;

/**
 * @author flower
 */
public class PlacesMediator extends Mediator {

    [Inject]
    public var view:PlacesList;
    public var selectedChild:PerfectObject;

    override public function onRegister():void {
        view.visible = false;
        addHandler(PerfectEvents.PLACES_LOADED, initList);
        sendMessage(PerfectEvents.REFRESH_PLACES);
        view.addEventListener(FlexEvent.STATE_CHANGE_COMPLETE, stateChangeCompleteHandler);
    }

    override public function onRemove():void {
    }

    private function onChange(p:PerfectObject):void {
        view.dataList.invalidateProperties();
    }

    private function initList(dc:DataCollection = null):void {
        view.objects = dc.toArrayList();
        for each (var p:PerfectObject in dc) {
            view.objects.itemUpdated(p);
        }
        view.visible = true;
    }

    private function dataListClickHandler(event:MouseEvent):void {
        if (!view.dataList.selectedItem) return;
        if (view.dataList.selectedItem != selectedChild) {
            selectedChild = view.dataList.selectedItem;
            if (selectedChild.mainType == PerfectObject.COLLECTION) {
                sendMessage(PerfectEvents.PLACES_LOADED, selectedChild);
            } else {
                sendMessage(PerfectEvents.PERFECT_SELECT, selectedChild);
            }
        }
    }

    private function stateChangeCompleteHandler(event:FlexEvent):void {
        view.panelsTabBar.gfx.txHeader.text = "Поля и полигоны";
        if (view.currentState == "show") {
            addHandler(PerfectEvents.PERFECT_CHANGE, onChange);
            view.dataList.addEventListener(MouseEvent.CLICK, dataListClickHandler);
        } else {
            removeHandler(PerfectEvents.PERFECT_CHANGE, onChange);
            view.dataList.removeEventListener(MouseEvent.CLICK, dataListClickHandler);
        }
    }
}
}