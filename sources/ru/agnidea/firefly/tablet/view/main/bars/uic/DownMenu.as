/**
 * Created by flower on 28.10.14.
 */
package ru.agnidea.firefly.tablet.view.main.bars.uic {
import flash.events.MouseEvent;

import mx.core.UIComponent;

import ru.agnidea.firefly.Core;
import ru.agnidea.firefly.core.AppState;
import ru.agnidea.firefly.tablet.events.GuiEvents;
import ru.agnidea.utils.cmToPx;

public class DownMenu extends UIComponent {
    public function DownMenu() {
        gfx = new GfxDownMenu();
        gfx.width = cmToPx(2.6);
        gfx.scaleY = gfx.scaleX;
        this.width = gfx.width;
        this.height = gfx.height;
        gfx.mcCount.visible = false;
        addChild(gfx);
        gfx.addEventListener(MouseEvent.CLICK, gfx_clickHandler);
    }

    public var gfx:GfxDownMenu;

    public function showCount(num:int):void {
        gfx.mcCount.visible = true;
        gfx.mcCount.txCount.text = num.toString();
        gfx.mcCount.gotoAndPlay(1);
    }

    public function hideCount():void {
        gfx.mcCount.visible = false;
        gfx.mcCount.stop();
    }

    private function gfx_clickHandler(event:MouseEvent):void {
        AppState.unreadMessges = 0;
        hideCount();
        Core.call(GuiEvents.SHOW_CHAT, true);
    }
}
}
