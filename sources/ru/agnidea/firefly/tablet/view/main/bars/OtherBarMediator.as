package ru.agnidea.firefly.tablet.view.main.bars {
import flash.events.MouseEvent;

import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.tablet.events.GuiEvents;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class OtherBarMediator extends Mediator {

    [Inject]
    public var view:OtherBars;

    override public function onRegister():void {
//        view.leftBox.addEventListener(MouseEvent.CLICK, leftClickHandler);
//        view.rightBox.addEventListener(MouseEvent.CLICK, rightClickHandler);
        view.settings.addEventListener(MouseEvent.CLICK, settingsClickHandler);
        view.centerComponent.addEventListener(MouseEvent.CLICK, settingsClickHandler);
    }


    private function settingsClickHandler(event:MouseEvent):void {
        sendMessage(GuiEvents.SHOW_SETTINGS, true);
    }




}
}