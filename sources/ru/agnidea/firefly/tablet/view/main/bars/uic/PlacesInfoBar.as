/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.tablet.view.main.bars.uic {
import flash.events.MouseEvent;

import mx.core.UIComponent;

import ru.agnidea.firefly.core.vo.PerfectObject;

public class PlacesInfoBar extends UIComponent {
    public function PlacesInfoBar() {
        addChild(gfx);
        gfx.h1.addEventListener(MouseEvent.CLICK, showInfo);
        gfx.h2.addEventListener(MouseEvent.CLICK, showAction);
        gfx.h3.addEventListener(MouseEvent.CLICK, showHistory);
    }

    public var changeView:Function;
    public var gfx:GfxInfoBarLeft = new GfxInfoBarLeft();

    public function set data(value:PerfectObject):void {
        gfx.label.text = value.name;
    }

    public function set close(close:Function):void {
        gfx.close.addEventListener(MouseEvent.CLICK, close);
    }

    override public function setLayoutBoundsSize(width:Number, height:Number, postLayoutTransform:Boolean = true):void {
        super.setLayoutBoundsSize(width, height, postLayoutTransform);
        gfx.width = this.width;
        gfx.scaleY = gfx.scaleX;
        this.height = gfx.height;
    }

    public function change(num:int):void {
        if (changeView != null) changeView(num);
        gfx.gotoAndStop(num);
    }

    private function showHistory(event:MouseEvent):void {
        change(3);
    }

    private function showAction(event:MouseEvent):void {
        change(2);
    }

    private function showInfo(event:MouseEvent):void {
        change(1);
    }
}
}
