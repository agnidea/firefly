package ru.agnidea.firefly.tablet.view.main.bars {
import flash.events.MouseEvent;

import mx.collections.ArrayList;
import mx.events.FlexEvent;

import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.firefly.tablet.events.PerfectEvents;
import ru.agnidea.firefly.tablet.gis.objects.Player;
import ru.agnidea.utils.DataCollection;

/**
 * @author flower
 */
public class PlayersMediator extends Mediator {

    [Inject]
    public var view:PlayersList;
    public var selectedChild:PerfectObject;
    private var allPerfect:DataCollection;
    private var selected:PerfectObject;

    override public function onRegister():void {
        addHandler(PerfectEvents.PLAYERS_LOADED, onData);
        addHandler(PerfectEvents.PERFECT_CHANGE, onPerfectChange);
        sendMessage(PerfectEvents.REFRESH_PLAYERS);
        view.addEventListener(FlexEvent.STATE_CHANGE_COMPLETE, stateChangeCompleteHandler);

    }

    override public function onRemove():void {

    }

    private function onPerfectChange(p:PerfectObject):void {
        if (p.mainType != PerfectObject.PLAYER) return;
        view.dataList.dataProvider.itemUpdated(p);
    }

    private function onPlayerSelect(angel:Player):void {
        for each (var perfect:PerfectObject in view.dataList.dataProvider) {
            if (perfect.uid == angel.perfect.uid) {
                view.dataList.selectedItem = perfect
            }
        }
    }

    private function onData(allPerfect:DataCollection):void {
        this.allPerfect = allPerfect;
        view.objects = new ArrayList(allPerfect.toArray());
    }


    private function dataListClickHandler(event:MouseEvent):void {
        if (!view.dataList.selectedItem) return;
//        if (view.dataList.selectedItem != selectedChild) {
            selectedChild = view.dataList.selectedItem;
            sendMessage(PerfectEvents.PERFECT_SELECT, selectedChild);
//        }
    }


    private function stateChangeCompleteHandler(event:FlexEvent):void {
        view.panelsTabBar.gfx.txHeader.text = "Подвижные объекты"
        if (view.currentState == "show") {
            addHandler(PerfectEvents.PLAYER_SELECT, onPlayerSelect);
            view.dataList.addEventListener(MouseEvent.CLICK, dataListClickHandler);
        } else {
            removeHandler(PerfectEvents.PLAYER_SELECT, onPlayerSelect);
            view.dataList.removeEventListener(MouseEvent.CLICK, dataListClickHandler);
        }
    }
}
}