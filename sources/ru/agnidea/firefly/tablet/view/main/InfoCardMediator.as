package ru.agnidea.firefly.tablet.view.main {
import flash.display.Stage;
import flash.events.MouseEvent;
import flash.geom.Point;

import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.Core;
import ru.agnidea.firefly.core.AppState;
import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.firefly.tablet.events.GuiEvents;
import ru.agnidea.firefly.tablet.events.NetworkEvents;
import ru.agnidea.firefly.tablet.events.PerfectEvents;
import ru.agnidea.firefly.tablet.gis.objects.Player;
import ru.agnidea.firefly.tablet.gis.objects.PolyPlace;
import ru.agnidea.firefly.tablet.view.QuickEdit;
import ru.agnidea.utils.DateFire;
import ru.agnidea.utils.cmToPx;

import starling.core.Starling;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class InfoCardMediator extends Mediator {

    public static var stage:Stage;
    [Inject]
    public var gfx:GfxObjectCard;
//    private var bitmap:Bitmap;
//    private var sprite:Sprite;
    private var startX:uint = cmToPx(2);
    private var currentPerfect:PerfectObject;
    private var lastSave:Point;
    private var showed:Boolean = false;
    private var utime:String;
    private var userMove:Boolean = false;

    override public function onRegister():void {
        addHandler(PerfectEvents.POLY_SELECT, onPolygonSelect);
        addHandler(PerfectEvents.PERFECT_SELECT, onPerfectSelect);
        addHandler(PerfectEvents.PERFECT_CHANGE, onPerfectSelect);
        addHandler(PerfectEvents.PLAYER_SELECT, onAngelSelect);
        addHandler(NetworkEvents.GMS_UPDATE, onGmsUpdate);

        addHandler(PerfectEvents.SOUL_INFO, onPerfectSelect);
        gfx.width = cmToPx(5);
        gfx.scaleY = gfx.scaleX;
//        trace(gfx.width, gfx.height);
//        var cm:int = cmToPx(1.3);
//        bitmap = new Bitmap(new BitmapData(gfx.width + cm * 2, gfx.height + cm * 2, true, 0x00000000));
//        sprite = new Sprite();
//        sprite.addChild(bitmap);
//        sprite.addChild(gfx.buttons);
        gfx.addEventListener(MouseEvent.MOUSE_DOWN, sprite_mouseDownHandler);
        gfx.addEventListener(MouseEvent.MOUSE_UP, sprite_mouseUpHandler);
        gfx.photo.stop();
        gfx.btOpen.addEventListener(MouseEvent.CLICK, openInfo);
        gfx.btCenter.addEventListener(MouseEvent.CLICK, goCenter);
        gfx.btClose.addEventListener(MouseEvent.CLICK, closeHandler);
        addHandler(GuiEvents.SHOW_MAIN, alginToMap);
        addHandler(GuiEvents.SHOW_CHAT, alginToView);
        addHandler(GuiEvents.SHOW_EDIT, alginToView);
        addHandler(GuiEvents.SHOW_SETTINGS, closeHandler);
        addHandler(GuiEvents.INFOCARD_CLOSE, closeHandler);

        gfx.txName.mouseEnabled = false;
        gfx.txVar1.mouseEnabled = false;
        gfx.txVar2.mouseEnabled = false;
        gfx.txVar3.mouseEnabled = false;
        gfx.txVar4.mouseEnabled = false;
        gfx.txValue1.mouseEnabled = false;
        gfx.txValue2.mouseEnabled = false;
        gfx.txValue3.mouseEnabled = false;
        gfx.txValue4.mouseEnabled = false;
        gfx.photo.mouseEnabled = false;


    }

    override public function onRemove():void {

    }

    private function onGmsUpdate(e:* = null):void {
        if (gfx.parent && utime != currentPerfect.updateTime) {
            onPerfectSelect(currentPerfect);
        }
    }

    private function alginToView(value:Object = null):void {
        showed = gfx.parent;
        if (showed && !lastSave) {
            lastSave = new Point(gfx.x, gfx.y);
        }
        gfx.x = 0;
        gfx.y = cmToPx(3.3);
        gfx..visible = true;
        gfx.btClose.visible = true;

    }

    private function alginToMap(value:Object = null):void {
        if (lastSave) {
            gfx.x = lastSave.x;
            gfx.y = lastSave.y;
            lastSave = null;
        } else {
            gfx.x = startX;
            gfx.y = cmToPx(3);
        }
        gfx.btOpen.visible = false;
        gfx.btClose.visible = true;
    }

    private function onPerfectSelect(perfect:PerfectObject):void {

        utime = perfect.updateTime;
        ;
        currentPerfect = perfect;
        gfx.txName.text = perfect.name;
        gfx.photo.gotoAndStop(perfect.photo);
        if (perfect.mainType == PerfectObject.PLAYER) {
            startX = cmToPx(2);
            gfx.txVar1.text = "скорость";
            gfx.txValue1.text = perfect.speed + " км/ч";
            gfx.txVar2.text = "id:";
            gfx.txValue2.text = perfect.uid;
            gfx.txVar3.text = "дата";
            gfx.txValue3.text = DateFire.dateRu(perfect.date);
            gfx.txVar4.text = "время";
            gfx.txValue4.text = DateFire.time(perfect.date);
        } else if (perfect.mainType == PerfectObject.POLY) {
            startX = Core.view.stage.stageWidth - cmToPx(7);
            gfx.txVar1.text = ""; //"плошать";
            gfx.txValue1.text = ""; //perfect.sizeGa + " га";
            gfx.txVar2.text = ""; //культура";
            gfx.txValue2.text = ""; //perfect.getCultureClass().name;
            gfx.txVar3.text = ""; //"дата посева";

            if (perfect.startDate) gfx.txValue3.text = ""; //DateFire.dateRu(perfect.startDate);
            else gfx.txValue3.text = "";
            gfx.txVar4.text = ""; //"дата сбора";
            if (perfect.startDate) gfx.txValue4.text = ""; //DateFire.dateRu(perfect.endDate);
            else gfx.txValue4.text = "";

        } else if (perfect.mainType == PerfectObject.SOUL) {
            startX = cmToPx(2);
            gfx.txVar1.text = "";
            gfx.txValue1.text = perfect.data.info;
            gfx.txVar2.text = "дата";
            gfx.txValue2.text = DateFire.dateRu(perfect.date);
            gfx.txVar3.text = "время";
            gfx.txValue3.text = DateFire.time(perfect.date);
            gfx.txVar4.text = "";
            gfx.txValue4.text = "";//perfect.center.lat.toString()+" "+perfect.center.lng.toString();

        }
        if (!userMove)
        if (AppState.infoCardState != "map") {
            alginToView();
        } else {
            gfx.x = startX;
            gfx.y = cmToPx(3);
            if (perfect.mainType == PerfectObject.SOUL) {
                gfx.btOpen.visible = true;
                gfx.btCenter.visible = false;
            } else {
                gfx.btCenter.visible = true;
                gfx.btOpen.visible = true;
                gfx.btClose.visible = true;
            }
        }
        if (!gfx.parent) {
            stage.addChild(gfx)
        }

    }

    private function onPolygonSelect(poly:PolyPlace):void {
        onPerfectSelect(poly.perfect);
    }

    private function onAngelSelect(angel:Player):void {

        var p:Point = angel.layer.localToGlobal(new Point(angel.body.x, angel.body.y));
//        trace(p.x > Core.view.stage.stageWidth / 2, p.x, Core.view.stage.stageWidth / 2);
        if (p.x > Core.view.stage.stageWidth / 2) {
            startX = cmToPx(2);
        } else {
            startX = Core.view.stage.stageWidth - cmToPx(7);
        }
        onPerfectSelect(angel.perfect);
    }

    private function closeHandler(event:Object = null):void {
        gfx.stopDrag();
        userMove = false;
        if (gfx.parent) {
            stage.removeChild(gfx);
        }
    }

    private function onEditComplete(newValue:String):void {
        currentPerfect.name = newValue;
        currentPerfect.saveQueued();
        sendMessage(PerfectEvents.PERFECT_CHANGE, currentPerfect);
    }

    private function openInfo(event:MouseEvent):void {
        gfx.stopDrag();
        gfx.x = cmToPx(0.5);
        ;
        gfx.y = cmToPx(0.1);
        AppState.currentPerfect = currentPerfect;
        QuickEdit.field.start(currentPerfect.name, "Смена имени объекта:", onEditComplete);
    }

    private function goCenter(event:MouseEvent):void {
        gfx.stopDrag();
        if (AppState.hideGis)
            sendMessage(GuiEvents.SHOW_MAIN);
        sendMessage(PerfectEvents.PERFECT_SELECT, currentPerfect);
        alginToMap();
    }

    private function sprite_mouseDownHandler(event:MouseEvent):void {
        if (AppState.infoCardState != "map") return;
        gfx.startDrag();
        Starling.current.root.touchable = false;
    }

    private function sprite_mouseUpHandler(event:MouseEvent):void {
        if (AppState.infoCardState != "map") return;
        gfx.stopDrag();
        userMove = true;
        Starling.current.root.touchable = true;
    }
}
}