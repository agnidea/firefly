package ru.agnidea.firefly.proto.model {
import org.mvcexpress.mvc.Proxy;

import ru.agnidea.firefly.proto.view.actions.ActionVO;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class HistoryProxy extends Proxy {

    public function HistoryProxy() {
    }

    public var actions:Array = [];

    override protected function onRegister():void {

    }

    override protected function onRemove():void {
    }

    public function addNewAction(actionVO:ActionVO):void {
        actions.push(actionVO);
    }
}
}