/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.proto.model {
import com.mapquest.LatLng;

public class RemoteGpsData {
    public function RemoteGpsData() {
    }

    public var latlon:LatLng;
    public var name:String;

    public function initLatLon(o:Object) {
        latlon = new LatLng();
        latlon.lat = o.lat;
        latlon.lng = o.lng;
    }
}
}
