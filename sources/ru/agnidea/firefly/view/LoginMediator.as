package ru.agnidea.firefly.view {
import flash.desktop.NativeApplication;
import flash.events.MouseEvent;
import flash.net.URLRequest;
import flash.net.navigateToURL;

import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.Core;
import ru.agnidea.firefly.core.VersionChecker;
import ru.agnidea.firefly.core.vo.User;
import ru.agnidea.firefly.tablet.events.GuiEvents;
import ru.agnidea.firefly.tablet.events.NetworkEvents;
import ru.agnidea.utils.doLater;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class LoginMediator extends Mediator {

    [Inject]
    public var view:LoginView;

    override public function onRegister():void {
        if (VersionChecker.checked) {
            if (VersionChecker.needUpdate) showUpdate();
        } else {
            addHandler(GuiEvents.NEED_UPDATE, showUpdate);
        }

        view.textInputLogin.text = Core.settings.data.login;
        view.textInputPassword.text = Core.settings.data.pass;
        view.passBox.selected = Core.settings.data.hidepass;

        view.textInputLogin.validateNow();
        view.textInputLogin.validateDisplayList();
        view.textInputLogin.validateSize();
//        view.textInputLogin.setCurrentState();
        view.stage.focus = view.textInputLogin;
//        view.textInputPassword.validateDisplayList();
//        view.textInputPassword.validateNow();

        view.passBox.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
        view.enterButton.addEventListener(MouseEvent.CLICK, clickHandler);
    }

    override public function onRemove():void {

    }

    public function showUpdate(o:Object = null):void {
        view.updateButton.visible = true;
        view.updateButton.label = view.updateButton.label + VersionChecker.remoteBuild;
        view.updateButton.addEventListener(MouseEvent.CLICK, updateClickHandler);
    }

    private function updateClickHandler(event:MouseEvent):void {
        navigateToURL(new URLRequest("itms-services://?action=download-manifest&url=https://dl.dropboxusercontent.com/s/r79b690m7fsbqvr/firefly.plist"));
        sendMessage(GuiEvents.SHOW_UPDATE);
        doLater(NativeApplication.nativeApplication.exit, 200);
    }

    private function mouseUpHandler(event:MouseEvent):void {
        Core.settings.saveHidePass(view.passBox.selected);
    }

    private function clickHandler(event:MouseEvent):void {
        User.login = view.textInputLogin.text;
        User.pass = view.textInputLogin.text;

        sendMessage(NetworkEvents.LOGIN);
    }
}
}