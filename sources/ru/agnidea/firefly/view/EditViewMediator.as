package ru.agnidea.firefly.view {
import flash.events.Event;
import flash.events.MouseEvent;

import mx.events.FlexEvent;

import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.core.AppState;
import ru.agnidea.firefly.core.vo.CultureVO;
import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.firefly.tablet.chat.model.ChatEvents;
import ru.agnidea.firefly.tablet.chat.model.UpdateMessageGenerator;
import ru.agnidea.firefly.tablet.events.GuiEvents;
import ru.agnidea.firefly.tablet.events.PerfectEvents;
import ru.agnidea.utils.DateFire;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class EditViewMediator extends Mediator {

    [Inject]
    public var view:EditView;
    private var currentPerfect:PerfectObject;

    override public function onRegister():void {
        view.backButton.addEventListener(MouseEvent.CLICK, backClickHandler);
        view.saveButton.addEventListener(MouseEvent.CLICK, saveClickHandler);
        view.addEventListener(Event.ADDED_TO_STAGE, view_addedToStageHandler);
        view.addEventListener(FlexEvent.STATE_CHANGE_COMPLETE, view_stateChangeCompleteHandler);
    }

    override public function onRemove():void {
    }

    private function backClickHandler(event:MouseEvent):void {
        sendMessage(GuiEvents.SHOW_MAIN);
    }

    private function view_addedToStageHandler(event:Event):void {
        currentPerfect = AppState.currentPerfect;
        view.nameInput.text = currentPerfect.name;
        var s:String
        s = view.currentState;
        if (currentPerfect.mainType == PerfectObject.PLAYER) {
            view.currentState = "player";
        } else {
            view.currentState = "polygon";
        }
        if (s == view.currentState) {
            view_stateChangeCompleteHandler()
        }

    }

    private function saveClickHandler(event:MouseEvent):void {
        var messageGen:UpdateMessageGenerator = new UpdateMessageGenerator();
        messageGen.perfectObject = currentPerfect;
        messageGen.checkName(currentPerfect.name, view.nameInput.text);
        currentPerfect.name = view.nameInput.text;
        if (currentPerfect.mainType == PerfectObject.POLY) {
            messageGen.checkStarDate(currentPerfect.startDate, view.startDateSpinner.selectedDate);
            currentPerfect.startDate = view.startDateSpinner.selectedDate;
            messageGen.checEndDate(currentPerfect.endDate, view.endDateSpinner.selectedDate);
            currentPerfect.endDate = view.endDateSpinner.selectedDate;
            messageGen.checkCultureID(currentPerfect.cultureID, view.cultureList.selectedItem.id);
            currentPerfect.cultureID = view.cultureList.selectedItem.id;
        } else {
            messageGen.checkTransportCatID(currentPerfect.transportCatID, view.tsCategoryList.selectedItem.id);
            currentPerfect.transportCatID = view.tsCategoryList.selectedItem.id;
            messageGen.checkTransportGosNumber(currentPerfect.transportGosNumber, view.gosNumInput.text);
            currentPerfect.transportGosNumber = view.gosNumInput.text;
        }
        currentPerfect.saveQueued();
        sendMessage(ChatEvents.SEND_PERFECT_UPDATE, messageGen);
        sendMessage(PerfectEvents.PERFECT_CHANGE, currentPerfect);
    }

    private function view_stateChangeCompleteHandler(event:FlexEvent = null):void {
        if (currentPerfect.mainType == PerfectObject.POLY) {
            if (!view.startCalloutButton) return;
            view.startCalloutButton.label = DateFire.dateRu(currentPerfect.startDate);
            view.startDateSpinner.selectedDate = currentPerfect.startDate;
            view.endCalloutButton.label = DateFire.dateRu(currentPerfect.endDate);
            view.endDateSpinner.selectedDate = currentPerfect.endDate;
            if (currentPerfect.startDate == null) {
                view.startCalloutButton.label = "";
                view.startDateSpinner.selectedDate = null;
            }
            var a:CultureVO = currentPerfect.getCultureClass();
            view.cultureCalloutButton.label = a.name;
            view.cultureList.selectedItem = currentPerfect.getCultureClass();

        } else if (currentPerfect.mainType == PerfectObject.PLAYER) {
            if (!view.tsCategoryCalloutButton) return;
            view.tsCategoryCalloutButton.label = currentPerfect.getTransportCatecoryClass().name;
            view.tsCategoryList.selectedItem = currentPerfect.getTransportCatecoryClass();
            view.gosNumInput.text = currentPerfect.transportGosNumber;
        }
    }
}
}