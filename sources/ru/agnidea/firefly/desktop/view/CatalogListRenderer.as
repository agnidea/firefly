/**
 * Created created in fire. With love.
 */
package ru.agnidea.firefly.desktop.view {
import flash.display.Sprite;
import flash.text.TextField;
import flash.text.TextFormat;

import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.firefly.tablet.common.ItemRendererBase;
import ru.agnidea.utils.adaptFontSizeToCurDPI;
import ru.agnidea.utils.cmToPx;
import ru.agnidea.utils.color.Colors;
import ru.agnidea.utils.color.FontName;

public class CatalogListRenderer extends ItemRendererBase {
    public function CatalogListRenderer() {
        mouseChildren = false;
//        addEventListener(MouseEvent.MOUSE_OVER, mouseOverHandler);
//        addEventListener(MouseEvent.MOUSE_OUT, mouseOutHandler);
        textField = new TextField();
        textField.embedFonts = true;
        format = new TextFormat();
        format.color = Colors.t.l900;
        format.font = FontName.ROBOTO_LIGHT_MX;
        format.size = adaptFontSizeToCurDPI(16.5);
        textField.setTextFormat(format);
        textField.defaultTextFormat = format;
        textField.selectable = false;
        textField.x = 5;
        hower = new Sprite();
        hower.visible = false;
        addChild(hower);
        addChild(textField);
        this.height = cmToPx(0.52);
        drawHower();
    }

    public var gfx:GfxPerfectItem;
    private var textField:TextField;
    private var format:TextFormat;
    private var hower:Sprite = new Sprite();
    private var tf:TextFormat;

    override public function set data(value:Object):void {
        super.data = value;
//        var classify:Classifier = value as Classifier;
        if (value)
            textField.text = value.name;
        selected = false;
    }

    override public function set showsCaret(value:Boolean):void {
        super.showsCaret = value;
        hower.visible = value;
    }

    override public function set selected(value:Boolean):void {
        super.selected = value;
        if (value) {
            format.font = FontName.ROBOTO_MEDIUM_MX;
            format.color = 0x000000;
        } else {

            format.font = FontName.ROBOTO_LIGHT_MX;
            if (data is PerfectObject && !data.saved) {
                format.font = FontName.ROBOTO_THIN_MX;
            }
            format.color = Colors.t.l900;
        }
        textField.setTextFormat(format);
    }

    override public function setLayoutBoundsSize(width:Number, height:Number, postLayoutTransform:Boolean = true):void {
        super.setLayoutBoundsSize(width, height, postLayoutTransform);
        textField.width = width;
        drawHower();
    }

    public function drawHower():void {
        hower.graphics.beginFill(Colors.t.a100);
        hower.graphics.drawRect(0, 0, width, height);
    }

//
//    private function mouseOverHandler(event:MouseEvent):void {
//        hower.visible = true;
//    }
//
//    private function mouseOutHandler(event:MouseEvent):void {
//        hower.visible = false;
//    }
}
}
