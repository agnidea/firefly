/**
 * Created by flower on 12.12.14.
 */
package ru.agnidea.firefly.desktop.view {
import ru.agnidea.firefly.core.AppState;
import ru.agnidea.firefly.mediators.CoreApplicationMediator;
import ru.agnidea.firefly.tablet.events.GisEvents;
import ru.agnidea.firefly.tablet.events.GuiEvents;

public class DesktopApplicationMediator extends CoreApplicationMediator {


    [Inject]
    public var view:DesktopApp;


    public var catalogView:CatalogView;

    override public function onRegister():void {
        app = view;
        super.onRegister();
        addHandler(GuiEvents.SHOW_MAIN, onMainShow);

    }

    private function onMainShow(o:Object = null):void {
        app.removeAllElements();
        AppState.infoCardState = "map";
        if (login) login = null;
        if (!catalogView) {
            mainGroup = catalogView = new CatalogView();
            mediatorMap.mediateWith(catalogView, CatalogViewMediator);
        }
        app.addElement(catalogView);

//        if (!starling) {
//            starling = new YStarling(app.stage);
//            mediatorMap.mediateWith(starling, YStarlingMediator);
//        }

        sendMessage(GisEvents.HIDE, false);
    }


}
}
