package ru.agnidea.firefly.desktop.view {


import com.gamua.flox.Entity;

import flash.events.MouseEvent;

import org.apache.flex.collections.ArrayList;
import org.mvcexpress.mvc.Mediator;

import ru.agnidea.firefly.Core;
import ru.agnidea.firefly.core.data.CatalogStore;
import ru.agnidea.firefly.core.data.Classifier;
import ru.agnidea.firefly.core.data.MainDataProxy;
import ru.agnidea.firefly.core.vo.CultureVO;
import ru.agnidea.firefly.core.vo.PerfectObject;
import ru.agnidea.firefly.core.vo.TSCategoryVO;
import ru.agnidea.firefly.tablet.events.DataEvents;
import ru.agnidea.firefly.tablet.events.PerfectEvents;

import spark.events.IndexChangeEvent;

/**
 * TODO:CLASS COMMENT
 * @author flower
 */
public class CatalogViewMediator extends Mediator {

    [Inject]
    public var view:CatalogView;

    public var data:MainDataProxy;
    public var catalog:CatalogStore;
    private var currentItem:Object;
    private var currentClassifeier:Classifier;

    override public function onRegister():void {
        data = Core.data;
        catalog = data.catalog;

        view.classifierList.dataProvider = new ArrayList(data.catalog.classifiers.toArray());
        view.classifierList.selectedIndex = 0;
        currentClassifeier = view.classifierList.selectedItem;

        view.classifierList.addEventListener(IndexChangeEvent.CHANGE, classChangeHandler);
        view.childList.addEventListener(IndexChangeEvent.CHANGE, itemChangeHandler);

        view.kmlButton.addEventListener(MouseEvent.CLICK, kmlClick);
        view.addTsCategoryButton.addEventListener(MouseEvent.CLICK, addTsCategory);
        view.addAgroCultureButton.addEventListener(MouseEvent.CLICK, addAgroCulture);

        addHandler(DataEvents.CATALOG_GEO_UPDATE, onGeoCatalogUpdate);
        addHandler(PerfectEvents.REFRESH_PLAYERS, onPlayersCatalogUpdate);

        view.editParams.saveButton.addEventListener(MouseEvent.CLICK, onSaveClick);
        view.editParams.deleteButton.addEventListener(MouseEvent.CLICK, onDeleteClick);


    }

    override public function onRemove():void {
    }

    public function onItemSelect(item:Object):void {
        currentItem = item as Entity;
        if (item is PerfectObject) {
            var perfect:PerfectObject = item as PerfectObject;
            view.editParams.setCurrentState(perfect.mainType);
            view.editParams.nameInput.text = perfect.name;
            if (perfect.mainType == PerfectObject.PLAYER) {

            }
        } else {
            view.editParams.setCurrentState("default");
        }
        if (view.childList.selectedItem != item) {
            view.childList.selectedItem = item;
        }
        view.editParams.nameInput.text = item.name;
        view.editParams.colorPicker.selectedColor = currentItem.color;
    }

    private function onPlayersCatalogUpdate(e:Object = null):void {
        view.childList.dataProvider = new ArrayList(currentClassifeier.childrens.toArray());
    }

    private function onGeoCatalogUpdate(o:Object = null):void {
        view.childList.dataProvider = new ArrayList(data.catalog.geoObjects.toArray());
        view.classifierList.selectedIndex = 0;
        currentClassifeier = view.classifierList.selectedItem as Classifier;
    }

    private function onDeleteClick(event:MouseEvent):void {
        currentClassifeier.childrens.removeItem(currentItem);
        view.childList.dataProvider = currentClassifeier.childrens.toArrayList();
        currentItem.destroyQueued();
        currentClassifeier.childrens.store();
        if (currentClassifeier.childrens.length) {
            onItemSelect(currentClassifeier.childrens.getItemAt(0));
        }
    }

    private function onSaveClick(event:MouseEvent):void {
        if (!currentItem) return;
        (currentItem as Object).name = view.editParams.nameInput.text;
        (currentItem as Object).color = view.editParams.colorPicker.selectedColor;

        if (currentItem is PerfectObject) {
            var perfect:PerfectObject = currentItem as PerfectObject;
            if (perfect.mainType == PerfectObject.PLAYER) {

            }
        }
        currentItem.saveQueued();
        currentClassifeier.childrens.store();
        view.childList.dataProvider.itemUpdated(currentItem);
    }

    private function addAgroCulture(event:MouseEvent):void {
        var acultrue:CultureVO = new CultureVO();
        acultrue.name = "Новая агрокультура " + catalog.agroCultures.length;
        catalog.agroCultures.add(acultrue);
        view.childList.dataProvider = data.catalog.agroCultures.toArrayList();
        view.classifierList.selectedIndex = 3;
        view.childList.selectedItem = acultrue;
        onItemSelect(acultrue);
    }

    private function addTsCategory(event:MouseEvent):void {
        var tsCategoryVO:TSCategoryVO = new TSCategoryVO();
        tsCategoryVO.name = "Новый тип ТС " + catalog.agroCultures.length;
        catalog.tsCategories.add(tsCategoryVO);
        view.childList.dataProvider = data.catalog.tsCategories.toArrayList();
        view.classifierList.selectedIndex = 2;
        view.childList.selectedItem = tsCategoryVO;
        onItemSelect(tsCategoryVO);
    }

    private function itemChangeHandler(event:IndexChangeEvent):void {
        if (view.childList.selectedItem) {
            onItemSelect(view.childList.selectedItem)
        }
    }

    private function classChangeHandler(event:IndexChangeEvent):void {
        if (view.classifierList.selectedItem) {
            var classifer:Classifier = view.classifierList.selectedItem as Classifier;
            currentClassifeier = classifer;
            view.childList.dataProvider = new ArrayList(classifer.childrens.toArray());
        }
    }

    private function kmlClick(event:MouseEvent):void {
        Core.data.kml.loadFormFile();
    }
}
}